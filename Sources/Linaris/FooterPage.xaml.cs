using CommunityToolkit.Maui.Core.Primitives;
using Model;
using Model.Stub;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace Linaris;

public partial class FooterPage : ContentView, INotifyPropertyChanged
{
    public new event PropertyChangedEventHandler PropertyChanged;

    private CustomTitle currentPlaying;

    public CustomTitle CurrentPlaying
    {
        get => currentPlaying;
        set
        {
            currentPlaying = value;
            OnPropertyChanged();
        }
    }

    private string playImage = "play.png";

    public string PlayImage
    {
        get => playImage;
        set
        {
            playImage = value;
            OnPropertyChanged();
        }
    }

    private string loopImage = "loop.png";

    public string LoopImage
    {
        get => loopImage;
        set
        {
            loopImage = value;
            OnPropertyChanged();
        }
    }

    private string shuffleImage = "rdm.png";

    public string ShuffleImage
    {
        get => shuffleImage;
        set
        {
            shuffleImage = value;
            OnPropertyChanged();
        }
    }

    private double volume = 1;

    public double Volume
    {
        get => volume;
        set
        {
            volume = value;
            OnPropertyChanged();
        }
    }

    private string position = "00:00:00";

    public string Position
    {
        get => position;
        set
        {
            position = value;
            OnPropertyChanged();
        }
    }

    private string duration = "00:00:00";

    public string Duration
    {
        get => duration;
        set
        {
            duration = value;
            OnPropertyChanged();
        }
    }

    private double sliderPosition = 0;

    public double SliderPosition
    {
        get => sliderPosition;
        set
        {
            sliderPosition = value;
            OnPropertyChanged();
        }
    }

    private readonly Manager manager = (Application.Current as App).Manager;

    public Manager Manager
    {
        get => manager;
    }

    public FooterPage()
    {
        InitializeComponent();

        music.StateChanged += Music_StateChanged;
        TimeSlider.ValueChanged += TimeSlider_ValueChanged;
        VolumeSlider.ValueChanged += VolumeSlider_ValueChanged;
        music.PositionChanged += Music_PositionChanged;
        CurrentPlaying = Manager.CurrentPlaying;
        BindingContext = this;
    }

    private void Music_PositionChanged(object sender, EventArgs e)
    {
        SliderPosition = music.Position.TotalSeconds / music.Duration.TotalSeconds;
        Position = music.Position.ToString(@"hh\:mm\:ss");
        Duration = music.Duration.ToString(@"hh\:mm\:ss");
    }

    private void VolumeSlider_ValueChanged(object sender, ValueChangedEventArgs e)
    {
        Volume = VolumeSlider.Value;
        music.Volume = Volume;
    }

    public void TimeSlider_ValueChanged(object sender, EventArgs e)
    {
        if ((TimeSlider.Value * music.Duration.TotalSeconds) - music.Position.TotalSeconds <= 1 && (TimeSlider.Value * music.Duration.TotalSeconds) - music.Position.TotalSeconds >= -1)
        {
            return;
        }
        TimeSpan PositionTimeSpan = TimeSpan.FromSeconds(TimeSlider.Value * music.Duration.TotalSeconds);
        music.SeekTo(PositionTimeSpan);
        Position = music.Position.ToString(@"hh\:mm\:ss");
        Duration = music.Duration.ToString(@"hh\:mm\:ss");
    }

    public void Music_StateChanged(object sender, EventArgs e)
    {
        if (music.CurrentState == MediaElementState.Paused || music.CurrentState == MediaElementState.Stopped)
        {
            PlayImage = "play.png";
        }
        else
        {
            PlayImage = "pause.png";
        }
    }

    public void RewindButton_Clicked(object sender, EventArgs e)
    {
        if (Manager.CurrentPlaylist == null || Manager.CurrentPlaying == null) return;
        Manager.PreviousTitle();
        CurrentPlaying = Manager.CurrentPlaying;
        Dispatcher.DispatchAsync(() =>
        {
            music.Source = Manager.CurrentPlaying.Path;
            music.SeekTo(TimeSpan.FromSeconds(0));
            Duration = music.Duration.ToString(@"hh\:mm\:ss");
        });
    }

    public void NextButton_Clicked(object sender, EventArgs e)
    {
        if (Manager.CurrentPlaylist == null || Manager.CurrentPlaying == null) return;
        Manager.NextTitle();
        CurrentPlaying = Manager.CurrentPlaying;
        Dispatcher.DispatchAsync(() =>
        {
            music.Source = Manager.CurrentPlaying.Path;
            music.SeekTo(TimeSpan.FromSeconds(0));
            Duration = music.Duration.ToString(@"hh\:mm\:ss");
        });
    }

    public void ShuffleButton_Clicked(object sender, EventArgs e)
    {
        Manager.Shuffle();
        if (ShuffleImage == "rdm.png")
        {
            ShuffleImage = "rdm2.png";
        } else {
            ShuffleImage = "rdm.png";
        }
    }

    public void LoopButton_Clicked(object sender, EventArgs e)
    {
        Manager.Loop();
        if (LoopImage == "loop.png")
        {
            LoopImage = "loop2.png";
        }
        else
        {
            LoopImage = "loop.png";
        }
    }

    public void OnCompleted(object sender, EventArgs e)
    {
        NextButton_Clicked(sender, e);
    }

    public void PlayButton_Clicked(object sender, EventArgs e)
    {
        if (music.CurrentState == MediaElementState.Paused || music.CurrentState == MediaElementState.Stopped)
        {
            music.Play();
        }
        else
        {
            music.Pause();
        }
    }

    protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
}