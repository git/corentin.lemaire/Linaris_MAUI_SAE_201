using Microsoft.Maui.Controls;
using Model;
using Model.Stub;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Linaris;

public partial class SearchBarView : ContentView
{
    Manager Manager { get; set; } = (Application.Current as App).Manager;

    public ObservableCollection<Album> Albums { get; set; } = new();

    public ObservableCollection<InfoTitle> InfoTitles { get; set; } = new();

    public ObservableCollection<Playlist> Playlists { get; set; } = new();

    public SearchBarView()
    {
        InitializeComponent();
        BindingContext = this;
    }

    public void SearchBar_TextChanged(object sender, EventArgs e)
    {
        string filterText = ((SearchBar)sender).Text;
        if (filterText.Length == 0)
        {
            ResetCollections();
            return;
        }
        SearchAll(filterText);
    }

    public void ResetCollections()
    {
        Albums.Clear();
        InfoTitles.Clear();
        Playlists.Clear();
    }

    public void SearchAll(string filterText)
    {
        Albums.Clear();
        foreach (var album in SearchAlbums(filterText))
        {
            Albums.Add(album);
        }

        InfoTitles.Clear();
        foreach (var infoTitle in SearchInfoTitles(filterText))
        {
            InfoTitles.Add(infoTitle);
        }

        Playlists.Clear();
        foreach (var playlist in SearchPlaylists(filterText))
        {
            Playlists.Add(playlist);
        }
    }

    public ObservableCollection<Album> SearchAlbums(string filterText)
    {
        ObservableCollection<Album> albumsFiltered = new ObservableCollection<Album>(Manager.GetAlbums().Where(x => x.Name.Contains(filterText, StringComparison.OrdinalIgnoreCase)));
        IEnumerable<Artist> artistsFiltered = new List<Artist>(Manager.GetArtists().Where(x => x.Name.Contains(filterText, StringComparison.OrdinalIgnoreCase)));

        foreach (var artist in artistsFiltered)
        {
            foreach (var album in Manager.AlbumsFromArtist[artist.Name])
            {
                Albums.Add(album);
            }
        }
        return albumsFiltered;
    }

    public ObservableCollection<InfoTitle> SearchInfoTitles(string filterText)
    {
        ObservableCollection<InfoTitle> infosTitlesFiltered = new ObservableCollection<InfoTitle>(Manager.GetInfoTitles().Where(x => x.Name.Contains(filterText, StringComparison.OrdinalIgnoreCase)));
        IEnumerable<Artist> artistsFiltered = new List<Artist>(Manager.GetArtists().Where(x => x.Name.Contains(filterText, StringComparison.OrdinalIgnoreCase)));

        foreach (var artist in artistsFiltered)
        {
            foreach (var infoTitle in Manager.InfoTitlesFromArtist[artist.Name])
            {
                InfoTitles.Add(infoTitle);
            }
        }
        return infosTitlesFiltered;
    }

    public ObservableCollection<Playlist> SearchPlaylists(string filterText)
    {
        return new ObservableCollection<Playlist>(Manager.GetPlaylists().Where(x => x.Name.Contains(filterText, StringComparison.OrdinalIgnoreCase)));
    }

    private async void SearchResult_Tapped(object sender, TappedEventArgs e)
    {
        if (((Frame)sender).BindingContext is Album album)
        {
            Manager.CurrentAlbum = album;
            await Navigation.PushAsync(new AlbumPage());
        }

        if (((Frame)sender).BindingContext is InfoTitle infoTitle)
        {
            Manager.CurrentInfoTitle = infoTitle;
            await Navigation.PushAsync(new InfoTitlePage());
        }

        if (((Frame)sender).BindingContext is Playlist playlist)
        {
            Manager.CurrentPlaylist = playlist;
            await Navigation.PushAsync(new PlaylistPage());
        }
    }
}
