﻿using CommunityToolkit.Maui.Core.Primitives;
using Model.Serialization;
using Model.Stub;
using System.Diagnostics;

namespace Linaris;

public partial class App : Application
{
    public Manager Manager { get; set; }

    public FooterPage FooterPage { get; set; }

    public TimeSpan MusicPosition { get; set; }

    public MediaElementState MediaState { get; set; }

    public App()
    {
        InitializeComponent();
        MusicPosition = TimeSpan.FromSeconds(0);
        MediaState = MediaElementState.Paused;
        Manager = new Manager(new LinqXmlSerialization(FileSystem.Current.AppDataDirectory));
        FooterPage = new FooterPage();
        MainPage = new AppShell();
    }

    protected override Window CreateWindow(IActivationState activationState)
    {

        var Window = base.CreateWindow(activationState);

        Window.Destroying += (sender, eventArgs) =>
        {
            Manager.SaveSerialization();
        };

        Window.Stopped += (sender, eventArgs) =>
        {
            Manager.SaveSerialization();
        };

        const int newHeight = 900;
        const int newWidth = 1800;
        const int minHeight = 800;
        const int minWidth = 400;

        Window.X = 0;
        Window.Y = 100;

        Window.Width = newWidth;
        Window.Height = newHeight;

        Window.MinimumHeight = minHeight;
        Window.MinimumWidth = minWidth;

        return Window;
    }
}
