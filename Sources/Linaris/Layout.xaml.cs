using CommunityToolkit.Maui.Views;

namespace Linaris;

public partial class Layout : ContentView
{
	public Layout()
	{
		InitializeComponent();
	}

	private async void Go_Home(object sender, EventArgs e)
    {
        await Navigation.PopToRootAsync();
    }

	private async void Go_Playlists(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new PlaylistsPage());
    }

	private async void Go_Back(object sender, EventArgs e)
    {
        await Navigation.PopAsync();
	}

	private async void Go_Files(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new LocalFilesPage());
    }
}