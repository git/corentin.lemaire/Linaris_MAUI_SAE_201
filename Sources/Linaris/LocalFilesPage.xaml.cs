using CommunityToolkit.Maui.Core.Primitives;
using CommunityToolkit.Maui.Views;
using Microsoft.Maui.Controls;
using Microsoft.Maui.Platform;
using Model;
using Model.Stub;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;

namespace Linaris;

public partial class LocalFilesPage : ContentPage
{
    private ObservableCollection<CustomTitle> customTitles = (Application.Current as App).Manager.GetCustomTitles();

    public ObservableCollection<CustomTitle> CustomTitles
    {
        get => customTitles;
    }

    private ObservableCollection<Playlist> playlists = (Application.Current as App).Manager.GetPlaylists();

    public ObservableCollection<Playlist> Playlists
    {
        get => playlists;
    }

    public LocalFilesPage()
    {
        InitializeComponent();
        BindingContext = this;
    }

    // Reset methods

    void ResetAll(object sender, EventArgs e)
    {
        ResetSubMenus();
    }

    void ResetSubMenus()
    {
        foreach (var CustomTitle in customTitles)
        {
            CustomTitle.IsSubMenuVisible = false;
        }
        ResetPlaylistMenu();
    }
    
    void ResetPlaylistMenu()
    {
        foreach (CustomTitle customTitle in CustomTitles)
        {
            customTitle.IsPlaylistMenuVisible = false;
            customTitle.IsNewPlaylistMenuVisible = false;
        }
    }


    // Add methods

    void AddCustomTitle(CustomTitle customTitle)
    {
        (Application.Current as App).Manager.AddCustomTitle(customTitle);
        customTitles = (Application.Current as App).Manager.GetCustomTitles();
        ResetAll(this, null);
    }

    private async void AddTitles(object sender, EventArgs e)
    {
        var results = await FilePicker.PickMultipleAsync(new PickOptions
        {
            FileTypes = new FilePickerFileType(
                new Dictionary<DevicePlatform, IEnumerable<string>>
                {
                    { DevicePlatform.WinUI, new [] { "*.mp3", "*.m4a" } },
                    { DevicePlatform.Android, new [] { "audio/*" } },
                    { DevicePlatform.iOS, new[] { "*.mp3", "*.aac", "*.aifc", "*.au", "*.aiff", "*.mp2", "*.3gp", "*.ac3" } }
                })
        });
        try
        {
            if (results.First() == null)
            {
                return;
            }
        }
        catch (Exception ex)
        {
            Debug.Print(ex.ToString());
            return;
        }

        foreach (var result in results)
        {
            string path = Path.Combine(FileSystem.Current.AppDataDirectory, "customs");
            if (!Path.Exists(path)) Directory.CreateDirectory(path);
            string fullPath = Path.Combine(path, result.FileName);
            if (!File.Exists(fullPath))
            {
                File.Copy(result.FullPath, fullPath);
                CustomTitle custom = new(result.FileName, "none.png", "", fullPath);
                if (!IsCustomTitleInCollection(custom))
                {
                    AddCustomTitle(custom);
                }
            }
        }
        (Application.Current as App).Manager.SaveSerialization();
    }

    async void AddToPlaylist(object sender, EventArgs e)
    {
        if (sender is Button button)
        {
            if (button.BindingContext is Playlist playlist)
            {
                if (button.Parent is StackLayout stack && stack.BindingContext is CustomTitle customTitle)
                {
                    if (playlist.HasCustomTitle(customTitle))
                    {
                        await DisplayAlert("Erreur !", "Ce morceau est d�j� dans cette playlist !", "OK");
                        return;
                    }
                    playlist.AddTitle(customTitle);
                    (Application.Current as App).Manager.SaveSerialization();
                    ResetAll(sender, e);
                }
            }
        }
    }

    void AddPlaylist(object sender, EventArgs e)
    {
        if (sender is Entry entry)
        {
            Playlist playlist = new(entry.Text, "", "none.png");
            if (!IsInPlaylists(playlist))
            {
                (Application.Current as App).Manager.AddPlaylist(playlist);
                playlists = (Application.Current as App).Manager.GetPlaylists();
            }
            if(entry.BindingContext is CustomTitle customTitle)
            {
                customTitle.IsNewPlaylistMenuVisible = false;
                entry.Text = "";
            }
        }
    }


    // Remove methods

    void RemoveCustomTitle(object sender, EventArgs e)
    {
        if (sender is Button button)
        {
            if (button.BindingContext is CustomTitle titleToRemove)
            {
                (Application.Current as App).Manager.RemoveCustomTitle(titleToRemove);
                customTitles.Remove(titleToRemove);
                File.Delete(titleToRemove.Path);
                (Application.Current as App).Manager.RemoveCustomTitleFromPlaylists(titleToRemove);
                (Application.Current as App).Manager.SaveSerialization();
            }
        }
    }


    // Show methods

    async void ShowSubMenu(object sender, EventArgs e)
    {
        if (sender is Image image && image.BindingContext is CustomTitle customTitle)
        {
            if (!customTitle.IsSubMenuVisible)
            {
                ResetAll(sender, e);
                customTitle.IsSubMenuVisible = true;
            }
            else
            {
                ResetSubMenus();
            }
            await MainScrollView.ScrollToAsync(image, ScrollToPosition.Center, true);
        }
    }

    void ShowPlaylistMenu(object sender, EventArgs e)
    {
        if (sender is Button button)
        {
            if (button.BindingContext is CustomTitle customTitle)
            {
                if (!customTitle.IsPlaylistMenuVisible)
                {
                    customTitle.IsPlaylistMenuVisible = true;
                }
            }
        }
    }

    void ShowNewPlaylistMenu(object sender, EventArgs e)
    {
        if (sender is Button button)
        {
            if (button.BindingContext is CustomTitle customTitle)
            {
                if (!customTitle.IsNewPlaylistMenuVisible)
                {
                    customTitle.IsNewPlaylistMenuVisible = true;
                }
            }
        }
    }


    // Change methods

    private async void ChangeImage(object sender, EventArgs e)
    {
        var result = await FilePicker.PickAsync(new PickOptions
        {
            PickerTitle = "Choisissez une nouvelle image !",
            FileTypes = FilePickerFileType.Images
        });

        if (result == null)
        {
            return;
        }

        if (sender is Button button)
        {
            if (button.BindingContext is CustomTitle customTitle)
            {
                customTitle.ImageURL = result.FullPath;
            }
        }
    }

    // Search methods

    bool IsCustomTitleInCollection(CustomTitle customTitle)
    {
        foreach(var custom in customTitles)
        {
            if (customTitle.Equals(custom))
            {
                return true;
            }
        }
        return false;
    }

    bool IsInPlaylists(Playlist playlist)
    {
        foreach (Playlist p in playlists)
        {
            if (p.Equals(playlist))
            {
                return true;
            }
        }
        return false;
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        GetFooterData();
    }

    protected override void OnDisappearing()
    {
        base.OnDisappearing();
        ResetSubMenus();
        SetFooterData();
        ContentView footer = this.FindByName<ContentView>("Footer");
        var musicElement = footer?.FindByName<MediaElement>("music");
        if (musicElement != null)
        {
            musicElement.Stop();
        }
    }

    private void GetFooterData()
    {
        FooterPage FooterPage = (Application.Current as App).FooterPage;
        Footer.CurrentPlaying = FooterPage.CurrentPlaying;
        Footer.PlayImage = FooterPage.PlayImage;
        Footer.LoopImage = FooterPage.LoopImage;
        Footer.ShuffleImage = FooterPage.ShuffleImage;
        Footer.Volume = FooterPage.Volume;
        Footer.Position = FooterPage.Position;
        Footer.Duration = FooterPage.Duration;
        Footer.SliderPosition = FooterPage.SliderPosition;

        // Place l'index de lecture de la musique � la position du slider
        var musicElement = Footer?.FindByName<MediaElement>("music");
        musicElement.Dispatcher.StartTimer(TimeSpan.FromMilliseconds(1000), () =>
        {
            musicElement.SeekTo((Application.Current as App).MusicPosition);
            if ((Application.Current as App).MediaState == MediaElementState.Playing)
            {
                musicElement.Play();
            }
            else
            {
                musicElement.Pause();
            }
            return false;
        });
    }

    public void SetFooterData()
    {
        FooterPage FooterPage = (Application.Current as App).FooterPage;
        FooterPage.CurrentPlaying = (Application.Current as App).Manager.CurrentPlaying;
        FooterPage.PlayImage = Footer.PlayImage;
        FooterPage.LoopImage = Footer.LoopImage;
        FooterPage.ShuffleImage = Footer.ShuffleImage;
        FooterPage.Volume = Footer.Volume;
        FooterPage.Position = Footer.Position;
        FooterPage.Duration = Footer.Duration;
        FooterPage.SliderPosition = Footer.SliderPosition;

        var musicElement = Footer?.FindByName<MediaElement>("music");
        (Application.Current as App).MusicPosition = musicElement.Position;
        (Application.Current as App).MediaState = musicElement.CurrentState;
    }


    // Others

    private void Play(object sender, EventArgs e)
    {
        if (sender is Button button && button.BindingContext is CustomTitle customTitle)
        {

            ContentView footer = this.FindByName<ContentView>("Footer");
            var musicElement = footer?.FindByName<MediaElement>("music");
            if (musicElement != null)
            {
                musicElement.Source = customTitle.Path;
                musicElement.SeekTo(TimeSpan.FromSeconds(0));
                musicElement.Play();
            }
            if (footer is FooterPage footerPage)
            {
                footerPage.CurrentPlaying = customTitle;
            }
            (Application.Current as App).Manager.CurrentPlaylist = null;
            (Application.Current as App).Manager.CurrentPlaying = customTitle;
        }
    }
}