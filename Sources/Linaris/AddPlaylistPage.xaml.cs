using Model;
using System.Collections.ObjectModel;

namespace Linaris;

public partial class AddPlaylistPage : ContentPage
{
    public Playlist NewPlaylist { get; } = new Playlist();

	public AddPlaylistPage()
	{
		InitializeComponent();
        BindingContext = NewPlaylist;
	}

    private async void ChangeImage(object sender, EventArgs e)
    {
        var result = await FilePicker.PickAsync(new PickOptions
        {
            PickerTitle = "Choisissez une nouvelle image !",
            FileTypes = FilePickerFileType.Images
        });

        if (result == null)
        {
            return;
        }

        if (sender is Image image)
        {
            if (image.BindingContext is Playlist playlist)
            {
                playlist.ImageURL = result.FullPath;
            }
        }
    }

    private void Cancel(object sender, EventArgs e)
    {
        Navigation.PopModalAsync();
    }

    private async void AddPlaylist(object sender, EventArgs e)
    {
        if ((Application.Current as App).Manager.GetPlaylistByName(NewPlaylist.Name) != null)
        {
            await DisplayAlert("Erreur !", "La playlist existe d�j�", "OK");
            return;
        }
        if (string.IsNullOrWhiteSpace(NewPlaylist.Name))
        {
            await DisplayAlert("Erreur !", "Le nom de la playlist ne doit pas �tre vide !", "OK");
            return;
        }
        (Application.Current as App).Manager.AddPlaylist(NewPlaylist);
        await Navigation.PopModalAsync();
    }
}