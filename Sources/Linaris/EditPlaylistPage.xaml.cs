using Model;
using Model.Stub;

namespace Linaris;

public partial class EditPlaylistPage : ContentPage
{

    private Playlist playlist;

    public Playlist Playlist { get => playlist; }

    public EditPlaylistPage()
    {
        InitializeComponent();
        CopyCurrent();
        BindingContext = Playlist;
    }

    private void CopyCurrent()
    {
        playlist = new Playlist();
        playlist.Name = (Application.Current as App).Manager.CurrentPlaylist.Name;
        playlist.Description = (Application.Current as App).Manager.CurrentPlaylist.Description;
        playlist.ImageURL = (Application.Current as App).Manager.CurrentPlaylist.ImageURL;
    }

    private async void ChangeImage(object sender, EventArgs e)
    {
        var result = await FilePicker.PickAsync(new PickOptions
        {
            PickerTitle = "Choisissez une nouvelle image !",
            FileTypes = FilePickerFileType.Images
        });

        if (result == null)
        {
            return;
        }

        if (sender is Image image && image.BindingContext is Playlist p)
        {
            p.ImageURL = result.FullPath;
        }
    }

    private void Cancel(object sender, EventArgs e)
    {
        Navigation.PopModalAsync();
    }

    private void EditPlaylist(object sender, EventArgs e)
    {
        (Application.Current as App).Manager.CurrentPlaylist.Name = playlist.Name;
        (Application.Current as App).Manager.CurrentPlaylist.Description = playlist.Description;
        (Application.Current as App).Manager.CurrentPlaylist.ImageURL = playlist.ImageURL;
        Navigation.PopModalAsync();
    }
}