﻿using CommunityToolkit.Maui.Core.Primitives;
using CommunityToolkit.Maui.Views;
using Model;
using System.Collections.ObjectModel;

namespace Linaris;

public partial class MainPage : ContentPage
{

    private readonly ObservableCollection<Album> albums;

    public ObservableCollection<Album> Albums
	{
		get => albums;
	}

	public MainPage()
	{
        InitializeComponent();
        albums = (Application.Current as App).Manager.GetAlbums();
        BindingContext = this;
    }

    async void GoToAlbum(object sender, EventArgs e)
	{
		if (sender is Image image && image.BindingContext is Album album)
        {
			(Application.Current as App).Manager.CurrentAlbum = album;
            await Navigation.PushAsync(new AlbumPage());
        }
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        GetFooterData();
    }

    protected override void OnDisappearing()
    {
        base.OnDisappearing();
        SetFooterData();
        ContentView footer = this.FindByName<ContentView>("Footer");
        var musicElement = footer?.FindByName<MediaElement>("music");
        if (musicElement != null)
        {
            musicElement.Stop();
        }
    }

    private void GetFooterData()
    {
        FooterPage FooterPage = (Application.Current as App).FooterPage;
        Footer.CurrentPlaying = FooterPage.CurrentPlaying;
        Footer.PlayImage = FooterPage.PlayImage;
        Footer.LoopImage = FooterPage.LoopImage;
        Footer.ShuffleImage = FooterPage.ShuffleImage;
        Footer.Volume = FooterPage.Volume;
        Footer.Position = FooterPage.Position;
        Footer.Duration = FooterPage.Duration;
        Footer.SliderPosition = FooterPage.SliderPosition;

        // Place l'index de lecture de la musique à la position du slider
        var musicElement = Footer?.FindByName<MediaElement>("music");
        musicElement.Dispatcher.StartTimer(TimeSpan.FromMilliseconds(100), () =>
        {
            musicElement.SeekTo((Application.Current as App).MusicPosition);
            if ((Application.Current as App).MediaState == MediaElementState.Playing)
            {
                musicElement.Play();
            }
            else
            {
                musicElement.Pause();
            }
            return false;
        });
    }

    public void SetFooterData()
    {
        FooterPage FooterPage = (Application.Current as App).FooterPage;
        FooterPage.CurrentPlaying = (Application.Current as App).Manager.CurrentPlaying;
        FooterPage.PlayImage = Footer.PlayImage;
        FooterPage.LoopImage = Footer.LoopImage;
        FooterPage.ShuffleImage = Footer.ShuffleImage;
        FooterPage.Volume = Footer.Volume;
        FooterPage.Position = Footer.Position;
        FooterPage.Duration = Footer.Duration;
        FooterPage.SliderPosition = Footer.SliderPosition;

        var musicElement = Footer?.FindByName<MediaElement>("music");
        (Application.Current as App).MusicPosition = musicElement.Position;
        (Application.Current as App).MediaState = musicElement.CurrentState;
    }
}

