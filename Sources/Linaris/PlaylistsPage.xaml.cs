using CommunityToolkit.Maui.Core.Primitives;
using CommunityToolkit.Maui.Views;
using Model;
using System.Collections.ObjectModel;

namespace Linaris;

public partial class PlaylistsPage : ContentPage
{
    private ObservableCollection<Playlist> playlists = (Application.Current as App).Manager.GetPlaylists();

    public ObservableCollection<Playlist> Playlists { get => playlists; }

    public PlaylistsPage()
	{
		InitializeComponent();
        BindingContext = this;
    }

    // Reset methods

    void ResetAll(object sender, EventArgs e)
    {
        ResetSubMenus(sender, e);
    }

    void ResetSubMenus(object sender, EventArgs e)
    {
        foreach (var Playlist in playlists)
        {
            Playlist.IsSubMenuVisible = false;
        }
    }


    // Add methods

    async void AddPlaylist(object sender, EventArgs e)
    {
        await Navigation.PushModalAsync(new AddPlaylistPage());
    }


    // Remove methods

    void RemovePlaylist(object sender, EventArgs e)
    {
        if (sender is Button button)
        {
            if (button.BindingContext is Playlist playlist)
            {
                playlists.Remove(playlist);
            }
        }
    }


    // Show methods

    void ShowSubMenu(object sender, EventArgs e)
    {
        if (sender is Image image)
        {
            if (image.BindingContext is Playlist playlist)
            {
                if (!playlist.IsSubMenuVisible)
                {
                    ResetAll(sender, e);
                    playlist.IsSubMenuVisible = true;
                }
                else
                {
                    ResetSubMenus(sender, e);
                }
            }
        }
    }


    // Change methods

    private async void ChangeImage(object sender, EventArgs e)
    {
        var result = await FilePicker.PickAsync(new PickOptions
        {
            PickerTitle = "Choisissez une nouvelle image !",
            FileTypes = FilePickerFileType.Images
        });

        if (result == null)
        {
            return;
        }

        if (sender is Button button)
        {
            if (button.BindingContext is Playlist playlist)
            {
                playlist.ImageURL = result.FullPath;
            }
        }
    }

    // Navigation

    async void GoToPlaylist(object sender, EventArgs e)
    {
        if (sender is Button button)
        {
            if (button.BindingContext is Playlist playlist)
            {
                (Application.Current as App).Manager.CurrentPlaylist = playlist;
            }
        }
        await Navigation.PushAsync(new PlaylistPage());
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        GetFooterData();
    }

    protected override void OnDisappearing()
    {
        base.OnDisappearing();
        SetFooterData();
        ContentView footer = this.FindByName<ContentView>("Footer");
        var musicElement = footer?.FindByName<MediaElement>("music");
        if (musicElement != null)
        {
            musicElement.Stop();
        }
    }

    private void GetFooterData()
    {
        FooterPage FooterPage = (Application.Current as App).FooterPage;
        Footer.CurrentPlaying = FooterPage.CurrentPlaying;
        Footer.PlayImage = FooterPage.PlayImage;
        Footer.LoopImage = FooterPage.LoopImage;
        Footer.ShuffleImage = FooterPage.ShuffleImage;
        Footer.Volume = FooterPage.Volume;
        Footer.Position = FooterPage.Position;
        Footer.Duration = FooterPage.Duration;
        Footer.SliderPosition = FooterPage.SliderPosition;

        // Place l'index de lecture de la musique � la position du slider
        var musicElement = Footer?.FindByName<MediaElement>("music");
        musicElement.Dispatcher.StartTimer(TimeSpan.FromMilliseconds(100), () =>
        {
            musicElement.SeekTo((Application.Current as App).MusicPosition);
            if ((Application.Current as App).MediaState == MediaElementState.Playing)
            {
                musicElement.Play();
            }
            else
            {
                musicElement.Pause();
            }
            return false;
        });
    }

    public void SetFooterData()
    {
        FooterPage FooterPage = (Application.Current as App).FooterPage;
        FooterPage.CurrentPlaying = (Application.Current as App).Manager.CurrentPlaying;
        FooterPage.PlayImage = Footer.PlayImage;
        FooterPage.LoopImage = Footer.LoopImage;
        FooterPage.ShuffleImage = Footer.ShuffleImage;
        FooterPage.Volume = Footer.Volume;
        FooterPage.Position = Footer.Position;
        FooterPage.Duration = Footer.Duration;
        FooterPage.SliderPosition = Footer.SliderPosition;

        var musicElement = Footer?.FindByName<MediaElement>("music");
        (Application.Current as App).MusicPosition = musicElement.Position;
        (Application.Current as App).MediaState = musicElement.CurrentState;
    }
}