using CommunityToolkit.Maui.Core.Primitives;
using CommunityToolkit.Maui.Views;
using Model;
using Model.Stub;

namespace Linaris;

public partial class PlaylistPage : ContentPage
{
	private readonly Manager Manager = (Application.Current as App).Manager;

	public PlaylistPage()
	{
		InitializeComponent();
		BindingContext = Manager.CurrentPlaylist;
	}

	private async void EditPlaylist(object sender, EventArgs e)
	{
        await Navigation.PushModalAsync(new EditPlaylistPage());
    }

	void RemoveCustomTitle(object sender, EventArgs e)
	{
		if (sender is MenuItem menuItem)
		{
			if (menuItem.BindingContext is CustomTitle customTitle)
            {
                Manager.GetPlaylistByName(Manager.CurrentPlaylist.Name).RemoveTitle(customTitle);
            }
		}
	}


    private void Play(object sender, EventArgs e)
	{
		if (sender is Button button)
		{
			if (button.BindingContext is Playlist playlist)
			{
				Manager.CurrentPlaylist = playlist;
				Manager.NextTitle();
                ContentView footer = this.FindByName<ContentView>("Footer");
                var musicElement = footer?.FindByName<MediaElement>("music");
				if (musicElement != null)
				{
                    if (Manager.CurrentPlaying == null) return;
                    musicElement.Source = Manager.CurrentPlaying.Path;
                    musicElement.SeekTo(TimeSpan.FromSeconds(0));
                    musicElement.Play();
                }
                if (footer is FooterPage footerPage)
                {
                    footerPage.CurrentPlaying = Manager.CurrentPlaying;
                }
            }
        }
	}

	private void RdmPlay(object sender, EventArgs e)
    {
		if (sender is Button button)
		{
			if (button.BindingContext is Playlist playlist)
			{
				Manager.CurrentPlaylist = playlist;
				Manager.Shuffle();
				Manager.NextTitle();
				ContentView footer = this.FindByName<ContentView>("Footer");
                var musicElement = footer?.FindByName<MediaElement>("music");
                if (musicElement != null)
                {
                    if (Manager.CurrentPlaying == null) return;
                    musicElement.Source = Manager.CurrentPlaying.Path;
                    musicElement.SeekTo(TimeSpan.FromSeconds(0));
                    musicElement.Play();
                    if (footer is FooterPage footerPage)
                    {
                        footerPage.ShuffleImage = "rdm2.png";
                        footerPage.CurrentPlaying = (Application.Current as App).Manager.CurrentPlaying;
                    }
                }
            }
		}
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        GetFooterData();
    }

    protected override void OnDisappearing()
    {
        base.OnDisappearing();
        SetFooterData();
        ContentView footer = this.FindByName<ContentView>("Footer");
        var musicElement = footer?.FindByName<MediaElement>("music");
        if (musicElement != null)
        {
            musicElement.Stop();
        }
    }

    private void GetFooterData()
    {
        FooterPage FooterPage = (Application.Current as App).FooterPage;
        Footer.CurrentPlaying = FooterPage.CurrentPlaying;
        Footer.PlayImage = FooterPage.PlayImage;
        Footer.LoopImage = FooterPage.LoopImage;
        Footer.ShuffleImage = FooterPage.ShuffleImage;
        Footer.Volume = FooterPage.Volume;
        Footer.Position = FooterPage.Position;
        Footer.Duration = FooterPage.Duration;
        Footer.SliderPosition = FooterPage.SliderPosition;

        // Place l'index de lecture de la musique � la position du slider
        var musicElement = Footer?.FindByName<MediaElement>("music");
        musicElement.Dispatcher.StartTimer(TimeSpan.FromMilliseconds(100), () =>
        {
            musicElement.SeekTo((Application.Current as App).MusicPosition);
            if ((Application.Current as App).MediaState == MediaElementState.Playing)
            {
                musicElement.Play();
            }
            else
            {
                musicElement.Pause();
            }
            return false;
        });
    }

    public void SetFooterData()
    {
        FooterPage FooterPage = (Application.Current as App).FooterPage;
        FooterPage.CurrentPlaying = (Application.Current as App).Manager.CurrentPlaying;
        FooterPage.PlayImage = Footer.PlayImage;
        FooterPage.LoopImage = Footer.LoopImage;
        FooterPage.ShuffleImage = Footer.ShuffleImage;
        FooterPage.Volume = Footer.Volume;
        FooterPage.Position = Footer.Position;
        FooterPage.Duration = Footer.Duration;
        FooterPage.SliderPosition = Footer.SliderPosition;

        var musicElement = Footer?.FindByName<MediaElement>("music");
        (Application.Current as App).MusicPosition = musicElement.Position;
        (Application.Current as App).MediaState = musicElement.CurrentState;
    }
}