using CommunityToolkit.Maui.Core.Primitives;
using CommunityToolkit.Maui.Views;
using Model;

namespace Linaris;

public partial class AlbumPage : ContentPage
{
	private Album album;

	public Album Album
	{
		get => album;
		set
		{
			album = value;
		}
	}

    public AlbumPage()
	{
		InitializeComponent();
        album = (Application.Current as App).Manager.CurrentAlbum;
        BindingContext = Album;

    }

    private async void GoToInfoTitle(object sender, EventArgs e)
    {
        if (sender is Frame frame && frame.BindingContext is InfoTitle infoTitle)
        {
            (Application.Current as App).Manager.CurrentInfoTitle = infoTitle;
            await Navigation.PushAsync(new InfoTitlePage());
        }
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        GetFooterData();
    }

    protected override void OnDisappearing()
    {
        base.OnDisappearing();
        SetFooterData();
        ContentView footer = this.FindByName<ContentView>("Footer");
        var musicElement = footer?.FindByName<MediaElement>("music");
        if (musicElement != null)
        {
            musicElement.Stop();
        }
    }

    private void GetFooterData()
    {
        FooterPage FooterPage = (Application.Current as App).FooterPage;
        Footer.CurrentPlaying = FooterPage.CurrentPlaying;
        Footer.PlayImage = FooterPage.PlayImage;
        Footer.LoopImage = FooterPage.LoopImage;
        Footer.ShuffleImage = FooterPage.ShuffleImage;
        Footer.Volume = FooterPage.Volume;
        Footer.Position = FooterPage.Position;
        Footer.Duration = FooterPage.Duration;
        Footer.SliderPosition = FooterPage.SliderPosition;

        // Place l'index de lecture de la musique � la position du slider
        var musicElement = Footer?.FindByName<MediaElement>("music");
        musicElement.Dispatcher.StartTimer(TimeSpan.FromMilliseconds(100), () =>
        {
            musicElement.SeekTo((Application.Current as App).MusicPosition);
            if ((Application.Current as App).MediaState == MediaElementState.Playing)
            {
                musicElement.Play();
            }
            else
            {
                musicElement.Pause();
            }
            return false;
        });
    }

    public void SetFooterData()
    {
        FooterPage FooterPage = (Application.Current as App).FooterPage;
        FooterPage.CurrentPlaying = (Application.Current as App).Manager.CurrentPlaying;
        FooterPage.PlayImage = Footer.PlayImage;
        FooterPage.LoopImage = Footer.LoopImage;
        FooterPage.ShuffleImage = Footer.ShuffleImage;
        FooterPage.Volume = Footer.Volume;
        FooterPage.Position = Footer.Position;
        FooterPage.Duration = Footer.Duration;
        FooterPage.SliderPosition = Footer.SliderPosition;

        var musicElement = Footer?.FindByName<MediaElement>("music");
        (Application.Current as App).MusicPosition = musicElement.Position;
        (Application.Current as App).MediaState = musicElement.CurrentState;
    }
}