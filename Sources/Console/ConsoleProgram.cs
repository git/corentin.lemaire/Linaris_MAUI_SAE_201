﻿using Model;
using Model.Stub;

namespace Console
{
    public class ConsoleProgram
    {
        public IDataManager DataManager { get; set; }

        public Manager Manager { get; set; }

        public Playlist P1 { get; set; }

        public Title? Current { get; set; }

        public ConsoleProgram()
        {
            DataManager = new StubManager();
            Manager = new(DataManager);
            P1 = Manager.Playlists.First();
            Current = P1.GetCurrentTitle();
        }

        // To avoid No entry point Found Exception
        public static void Main()
        {
            throw new NotSupportedException();
        }

        public void FonctionalTests()
        {
            // Playlist system

            Manager.AddPlaylist(new Playlist("MegaTeuf", "DescPlaylist", "ImagePlaylist"));

            System.Console.WriteLine(Current?.Name);

            Next(2);

            P1.AddTitle(new CustomTitle("Nouveau", "img.png", "infos", "path.mp3"));
            P1.Shuffle = true;

            Separator();

            Next(5);

            Separator();

            Previous(5);

            Separator();

            Next(2);

            Separator();

            Previous(10);

            // Serialization

            Manager.LoadSerialization();

            DisplaySerialization(Manager.GetPlaylists());
            DisplaySerialization(Manager.GetAlbums());
            DisplaySerialization(Manager.GetArtists());
            DisplaySerialization(Manager.GetCustomTitles());
            DisplaySerialization(Manager.GetInfoTitles());

            Artist newArtist = new();
            Manager.AddArtist(newArtist);
            Album album = new("Nouvel album", "nouveau.png", newArtist, "nouvelle desc", "nouvelles infos");
            Manager.AddAlbum(album);

            Separator();

            DisplaySerialization(Manager.GetAlbums());

            Manager.SaveSerialization();
            Manager.LoadSerialization();

            DisplaySerialization(Manager.GetPlaylists());
            DisplaySerialization(Manager.GetAlbums());
            DisplaySerialization(Manager.GetArtists());
            DisplaySerialization(Manager.GetCustomTitles());
            DisplaySerialization(Manager.GetInfoTitles());

            Manager.RemoveAlbum(album);

            Manager.SaveSerialization();
            Manager.LoadSerialization();

            DisplaySerialization(Manager.GetPlaylists());
            DisplaySerialization(Manager.GetAlbums());
            DisplaySerialization(Manager.GetArtists());
            DisplaySerialization(Manager.GetCustomTitles());
            DisplaySerialization(Manager.GetInfoTitles());
        }

        public void Next(int n)
        {
            for (int i = 0; i < n; i++)
            {
                P1.NextTitle();
                Current = P1.GetCurrentTitle();
                System.Console.WriteLine(Current?.Name);
            }
        }

        public void Previous(int n)
        {
            for (int i = 0; i < n; i++)
            {
                P1.PreviousTitle();
                Current = P1.GetCurrentTitle();
                System.Console.WriteLine(Current?.Name);
            }
        }

        public static void DisplaySerialization<T>(IEnumerable<T> objects)
        {
            foreach (var obj in objects)
            {
                System.Console.WriteLine(obj);
            }
        }

        public static void Separator()
        {
            System.Console.WriteLine("--------------------");
        }
    }
}