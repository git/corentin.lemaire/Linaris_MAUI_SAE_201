using Model;
using Model.Serialization;
using Model.Stub;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;
using System;

namespace TestUnitaires
{

    public class TU_StubManager
    {
        [Theory]
        [InlineData("R�ference")]
        public void TU_Methods(string? test)
        {

            Playlist p = new Playlist(test, "PlaceHolder", "place.holder");
            Album album = new Album(test, "place.holder", new Artist("test"), "PlaceHolder", "PlaceHolder");
            CustomTitle t = new CustomTitle(test, "test. mp3", "Banger", "path");
            Artist artist = new Artist(test);
            StubManager stub = new StubManager();
            InfoTitle info = new InfoTitle(test, "urstub.test", "infos", "desc", Genre.POP, 345);
            stub.AddCustomTitle(t);
            stub.AddPlaylist(p);
            stub.AddAlbum(album);
            stub.AddArtist(artist);
            stub.AddInfoTitle(info);
            Assert.Contains(t, stub.StubCustomTitle.CustomTitles);
            Assert.Contains(album, stub.StubAlbum.Albums);
            Assert.Contains(p, stub.StubPlaylist.Playlists);
            Assert.Contains(info, stub.StubInfoTitle.InfoTitles);
            Assert.Contains(artist, stub.StubArtist.Artists);
            stub.RemovePlaylist(p);
            stub.RemoveAlbum(album);
            stub.RemoveCustomTitle(t);
            stub.RemoveInfoTitle(info);
            stub.RemoveArtist(artist);
            Assert.DoesNotContain(t, stub.StubCustomTitle.CustomTitles);
            Assert.DoesNotContain(p, stub.StubPlaylist.Playlists);
            Assert.DoesNotContain(album, stub.StubAlbum.Albums);
            Assert.DoesNotContain(info, stub.StubInfoTitle.InfoTitles);
            stub.AddCustomTitle(t);
            stub.AddPlaylist(p);
            stub.AddAlbum(album);
            stub.AddArtist(artist);
            stub.AddInfoTitle(info);
            Assert.Equal(stub.GetAlbums(), stub.StubAlbum.Albums);
            Assert.Equal(stub.GetInfoTitles(), stub.StubInfoTitle.InfoTitles);
            Assert.Equal(stub.GetCustomTitles(), stub.StubCustomTitle.CustomTitles);
            Assert.Equal(stub.GetPlaylists(), stub.StubPlaylist.Playlists);
            Assert.Equal(stub.GetArtists(), stub.StubArtist.Artists);
            Assert.Equal(stub.GetAlbumByName(test), album);
            Assert.Equal(stub.GetPlaylistByName(test), p);
            Assert.Equal(stub.GetInfoTitleByName(test), info);
            Assert.Equal(stub.GetArtistByName(test), artist);
            Assert.True(stub.ExistsAlbum(album));
            Assert.True(stub.ExistsArtist(artist));
            Assert.True(stub.ExistsPlaylist(p));
            Assert.True(stub.ExistsCustomTitle(t));
            Assert.True(stub.ExistsInfoTitle(info));
            Assert.True(stub.ExistsAlbumByName(test));
            Assert.True(stub.ExistsArtistByName(test));
            Assert.True(stub.ExistsPlaylistByName(test));
            Assert.True(stub.ExistsInfoTitleByName(test));
            Assert.True(stub.ExistsCustomTitleByName(test));
            stub.RemovePlaylist(p);
            stub.RemoveAlbum(album);
            stub.RemoveCustomTitle(t);
            stub.RemoveInfoTitle(info);
            stub.RemoveArtist(artist);
            Assert.False(stub.ExistsAlbum(album));
            Assert.False(stub.ExistsArtist(artist));
            Assert.False(stub.ExistsPlaylist(p));
            Assert.False(stub.ExistsCustomTitle(t));
            Assert.False(stub.ExistsInfoTitle(info));
            Assert.False(stub.ExistsAlbumByName(test));
            Assert.False(stub.ExistsArtistByName(test));
            Assert.False(stub.ExistsPlaylistByName(test));
            Assert.False(stub.ExistsInfoTitleByName(test));
            Assert.False(stub.ExistsCustomTitleByName(test));
            stub.SaveSerialization();
            stub.LoadSerialization();
            StubManager.AddFeat(info, new Artist("artist2"));
            stub.AddCustomTitle(t);
            var res = stub.GetCustomTitleByPath(t.Path);
            Assert.Equal(t, res);
        }

        [Fact]
        public void TU_RemoveAddMultiple()
        {
            Artist a = new("artist");
            List<Album> albums = new()
            {
                new Album("album1", "img1.png", new Artist("artist"), "desc", "info"),
                new Album("album2", "img2.png", new Artist("artist"), "desc", "info"),
                new Album("album3", "img3.png", new Artist("artist"), "desc", "info")
            };
            List<Artist> artists = new()
            {
                new Artist("artist1"),
                new Artist("artist2"),
                new Artist("artist3")
            };
            List<InfoTitle> infoTitles = new()
            {
                new InfoTitle("it1", "img1.png", "desc", "info", Genre.POP, 0),
                new InfoTitle("it2", "img2.png", "desc", "info", Genre.POP, 0),
                new InfoTitle("it3", "img3.png", "desc", "info", Genre.POP, 0)
            };
            List<CustomTitle> customTitles = new()
            {
                new CustomTitle("ct1", "img1.png", "info", "path1.mp3"),
                new CustomTitle("ct2", "img2.png", "info", "path2.mp3"),
                new CustomTitle("ct3", "img3.png", "info", "path3.mp3")
            };
            List<Playlist> playlists = new()
            {
                new Playlist("nom1", "desc", "img1.png"),
                new Playlist("nom2", "desc", "img2.png"),
                new Playlist("nom3", "desc", "img3.png")
            };
            StubManager stubManager = new();
            stubManager.AddAlbums(albums);
            stubManager.AddArtists(artists);
            stubManager.AddInfoTitles(infoTitles);
            stubManager.AddCustomTitles(customTitles);
            stubManager.AddPlaylists(playlists);

            stubManager.RemoveAlbums(albums);
            stubManager.RemoveArtists(artists);
            stubManager.RemoveInfoTitles(infoTitles);
            stubManager.RemoveCustomTitles(customTitles);
            stubManager.RemovePlaylists(playlists);

            stubManager.AddArtist(a);
            stubManager.RemoveArtist(a);
        }
    }
    
}