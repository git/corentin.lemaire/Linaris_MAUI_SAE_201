using Model;
using Model.Serialization;
using Model.Stub;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;
using System;
using System.IO;

namespace TestUnitaires
{

    public class TU_Manager
    {
        [Theory]
        [InlineData("Reference")]
        public void TU_Methods(string? test)
        {
            Playlist p = new(test, "PlaceHolder", "place.holder");
            Album album = new(test, "place.holder", new Artist("test"), "PlaceHolder", "PlaceHolder");
            CustomTitle t = new(test, "test. mp3", "Banger", "path");
            Artist artist = new(test);
            Manager m = new(new LinqXmlSerialization(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data")));
            InfoTitle info = new(test, "url.test", "infos", "desc", Genre.POP, 345);
            m.AddCustomTitle(t);
            m.AddPlaylist(p);
            m.AddAlbum(album);
            m.AddArtist(artist);
            m.AddInfoTitle(info);
            Assert.Contains(t, m.CustomTitles);
            Assert.Contains(album, m.Albums);
            Assert.Contains(p, m.Playlists);
            Assert.Contains(info, m.InfoTitles);
            Assert.Contains(artist, m.Artists);
            m.RemovePlaylist(p);
            m.RemoveAlbum(album);
            m.RemoveCustomTitle(t);
            m.RemoveInfoTitle(info);
            Assert.DoesNotContain(t, m.CustomTitles);
            Assert.DoesNotContain(p, m.Playlists);
            Assert.DoesNotContain(album, m.Albums);
            Assert.DoesNotContain(info, m.InfoTitles);
            m.AddCustomTitle(t);
            m.AddPlaylist(p);
            m.AddAlbum(album);
            m.AddArtist(artist);
            m.AddInfoTitle(info);
            Assert.Equal(m.GetAlbums(), m.Albums);
            Assert.Equal(m.GetInfoTitles(), m.InfoTitles);
            Assert.Equal(m.GetCustomTitles(), m.CustomTitles);
            Assert.Equal(m.GetPlaylists(), m.Playlists);
            Assert.Equal(m.GetArtists(), m.Artists);
            Assert.Equal(m.GetAlbumByName(test), album);
            Assert.Equal(m.GetPlaylistByName(test), p);
            Assert.Equal(m.GetInfoTitleByName(test), info);
            Assert.Equal(m.GetArtistByName(test), artist);

            var ca = m.CurrentAlbum;
            var ci = m.CurrentInfoTitle;
            var cc = m.CurrentPlaying;
            var cp = m.CurrentPlaylist;

            Assert.Equal(m.CurrentAlbum, ca);
            Assert.Equal(m.CurrentInfoTitle, ci);
            Assert.Equal(m.CurrentPlaying, cc);
            Assert.Equal(m.CurrentPlaylist, cp);

            m.CurrentPlaylist = p;
            m.CurrentInfoTitle = info;
            m.CurrentPlaying = t;
            m.CurrentAlbum = album;

            Assert.Equal(p, m.CurrentPlaylist);
            Assert.Equal(info, m.CurrentInfoTitle);
            Assert.Equal(t, m.CurrentPlaying);
            Assert.Equal(album, m.CurrentAlbum);

            m.NextTitle();
            Assert.Null(m.CurrentPlaying);
            m.PreviousTitle();
            Assert.Null(m.CurrentPlaying);

            m.CurrentPlaylist = null;
            m.NextTitle();
            Assert.Null(m.CurrentPlaying);
            m.PreviousTitle();
            Assert.Null(m.CurrentPlaying);

            var cct = m.CurrentTitle();
            Assert.Null(cct);

            m.Loop();

            m.Shuffle();

            p.AddTitle(t);
            m.CurrentPlaylist = p;
            m.CurrentPlaying = t;
            cct = m.CurrentTitle();
            Assert.NotNull(cct);

            var loop = m.CurrentPlaylist.LoopTitle;
            m.Loop();
            Assert.NotEqual(loop, m.CurrentPlaylist.LoopTitle);

            var shuffle = m.CurrentPlaylist.Shuffle;
            m.Shuffle();
            Assert.NotEqual(shuffle, m.CurrentPlaylist.Shuffle);

            var custom = m.GetCustomTitleByPath(t.Path);
            Assert.Equal(t, custom);

            p.AddTitle(t);
            m.RemoveCustomTitleFromPlaylists(t);
            Assert.DoesNotContain(t, p.Titles);

        }

        [Theory]
        [InlineData("nom", "url2.png", "artist", "desc", "", Genre.POP, "")]
        public void TU_Update(string nom, string url, string artistName, string description, string info, Genre genre, string path)
        {
            Album album = new("album1", "img1.png", new Artist("artist1"), "desc", "info");
            Album album2 = new("album2", "img2.png", new Artist("artist1"), "desc", "info");

            Artist artist = new("artist1");
            Artist artist2 = new("artist2");

            InfoTitle infoTitle = new("it1", "img1.png", "desc", "info", Genre.RAP, 0);
            InfoTitle infoTitle2 = new("it12", "img12.png", "desc", "info", Genre.RAP, 0);

            CustomTitle customTitle = new("nom1", "img1.png", "info", "path1.mp3");
            CustomTitle customTitle2 = new("nom2", "img2.png", "info", "path2.mp3");

            Playlist playlist = new("nom1", "desc", "img1.png");
            Playlist playlist2 = new("nom2", "desc", "img2.png");

            Manager m = new(new LinqXmlSerialization(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data")));
                
            m.AddAlbum(album);
            m.AddAlbum(album2);
            m.AddArtist(artist);
            m.AddArtist(artist2);
            m.AddInfoTitle(infoTitle);
            m.AddInfoTitle(infoTitle2);
            m.AddCustomTitle(customTitle);
            m.AddCustomTitle(customTitle2);
            m.AddPlaylist(playlist);
            m.AddPlaylist(playlist2);

            Manager.UpdateAlbum(album, nom, url, new Artist(artistName), description, info);
            m.UpdateAlbumByName(album2.Name, nom, url, new Artist(artistName), description, info);

            Manager.UpdateArtist(artist, nom);
            m.UpdateArtistByName(artist2.Name, nom);

            Manager.UpdateInfoTitle(infoTitle, nom, url, info, new Artist(artistName), description, genre);
            m.UpdateInfoTitleByName(infoTitle2.Name, nom, url, info, new Artist(artistName), description, genre);

            Manager.UpdateCustomTitle(customTitle, nom, url, info, path);
            m.UpdateCustomTitleByPath(customTitle2.Path, nom, url, info, path);

            Manager.UpdatePlaylist(playlist, nom, description, url);
            m.UpdatePlaylistByName(playlist2.Name, nom, description, url);

            List<Album> albums = new()
            {
                album, album2
            };
            List<Artist> artists = new()
            {
                artist, artist2
            };
            List<InfoTitle> infoTitles = new()
            {
                infoTitle, infoTitle2
            };
            List<CustomTitle> customTitles = new()
            {
                customTitle, customTitle2
            };
            List<Playlist> playlists = new()
            {
                playlist, playlist2
            };

            foreach (Album a in albums)
            {
                Assert.Equal(nom, a.Name);
                Assert.Equal(url, a.ImageURL);
                Assert.Equal(artistName, a.Artist.Name);
                Assert.Equal(description, a.Description);
                Assert.Equal(info, a.Information);
            }
            foreach (Artist a in artists)
            {
                Assert.Equal(nom, a.Name);
            }
            foreach (InfoTitle it in infoTitles)
            {
                Assert.Equal(nom, it.Name);
                Assert.Equal(url, it.ImageURL);
                Assert.Equal(description, it.Description);
                Assert.Equal(info, it.Information);
                Assert.Equal(genre, it.Genre);
            }
            foreach (CustomTitle ct in customTitles)
            {
                Assert.Equal(nom, ct.Name);
                Assert.Equal(url, ct.ImageURL);
                Assert.Equal(info, ct.Information);
                Assert.Equal(path, ct.Path);
            }
            foreach (Playlist p in playlists)
            {
                Assert.Equal(nom, p.Name);
                Assert.Equal(description, p.Description);
                Assert.Equal(url, p.ImageURL);
            }

        }
    }
    
}