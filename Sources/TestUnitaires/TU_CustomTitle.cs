using Model;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;
using System.IO;
using System.Xml.Linq;
using System;

namespace TestUnitaires
{

    public class TU_CustomTitle
    {
        [Theory]
        [InlineData("Trajectoire","morceau1.png","Sortie : 2020", "Musique/test.mp3")]
        [InlineData(null, "morceau1.png", "Sortie : 2020", "Musique/test.mp3")]
        [InlineData("Trajectoire", null, "Sortie : 2020", "Musique/test.mp3")]
        [InlineData("Trajectoire", "morceau1.png", null, "Musique/test.mp3")]
        [InlineData("Trajectoire", "morceau1png", "Sortie : 2020", "Musique/test.mp3")]
        [InlineData("Trajectoire", "morceau1. png", "Sortie : 2020", "Musique/test.mp3")]
        [InlineData("Trajectoire", "morceau1.png", "Sortie : 2020", null)]
        public void TU_Attributes(string name, string url, string info, string path)
        {
            CustomTitle ct = new(name, url, info, path);
            Assert.True(ct.Name != null && ct.Name.Length < 75);
            Assert.True(ct.ImageURL != null && ct.ImageURL.Contains('.'));
            Assert.True(ct.Information != null && ct.Information.Length < 500);
            Assert.True(ct.Path != null && ct.Path.Contains('.'));
            var res = ct.IsSubMenuVisible;
            ct.IsSubMenuVisible = ct.IsSubMenuVisible;
            Assert.Equal(res, ct.IsSubMenuVisible);
            ct.IsSubMenuVisible = !ct.IsSubMenuVisible;
            Assert.NotEqual(res, ct.IsSubMenuVisible);

            res = ct.IsPlaylistMenuVisible;
            ct.IsPlaylistMenuVisible = ct.IsPlaylistMenuVisible;
            Assert.Equal(res, ct.IsPlaylistMenuVisible);
            ct.IsPlaylistMenuVisible = !ct.IsPlaylistMenuVisible;
            Assert.NotEqual(res, ct.IsPlaylistMenuVisible);

            res = ct.IsNewPlaylistMenuVisible;
            ct.IsNewPlaylistMenuVisible = ct.IsNewPlaylistMenuVisible;
            Assert.Equal(res, ct.IsNewPlaylistMenuVisible);
            ct.IsNewPlaylistMenuVisible = !ct.IsNewPlaylistMenuVisible;
            Assert.NotEqual(res, ct.IsNewPlaylistMenuVisible);

        }

        [Theory]
        [InlineData("Trajectoire", "morceau1.png", "Sortie : 2020", "Musique/test.mp3")]
        [InlineData(null, "morceau1.png", "Sortie : 2020", "Musique/test.mp3")]
        [InlineData("Trajectoire", null, "Sortie : 2020", "Musique/test.mp3")]
        [InlineData("Trajectoire", "morceau1.png", null, "Musique/test.mp3")]
        [InlineData("Trajectoire", "morceau1png", "Sortie : 2020", "Musique/test.mp3")]
        [InlineData("Trajectoire", "morceau1. png", "Sortie : 2020", "Musique/test.mp3")]
        [InlineData("Trajectoire", "morceau1.png", "Sortie : 2020", null)]
        public void TU_Methods(string name, string url, string info, string path)
        {
            CustomTitle ct = new(name, url, info, path);
            ct.GetHashCode();
            Assert.False(ct.Equals(null));
            Assert.False(ct.Equals(new Artist("artiste")));
            Assert.True(ct.Equals(new CustomTitle(name, url, info, path)));
            Assert.False(ct.Equals(new CustomTitle(name, url, info, "path")));
        }
    }

}