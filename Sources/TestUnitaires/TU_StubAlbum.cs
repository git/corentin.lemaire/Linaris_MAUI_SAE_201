using Model;
using Model.Stub;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;
using System.Collections.ObjectModel;

namespace TestUnitaires
{

    public class TU_StubAlbum
    {
        [Fact]
        public void TU_Methods()
        {
            StubAlbum stubAlbum = new StubAlbum();
            Artist artist = new Artist("Un mec");
            ObservableCollection<Album> collec = new ObservableCollection<Album>();
            Album album = new Album("Bon Album", "album1.png", artist, "une description", "des informations");
            stubAlbum.AddAlbum(album);
            Assert.Contains(album, stubAlbum.Albums);
            stubAlbum.RemoveAlbum(album);
            Assert.DoesNotContain(album, stubAlbum.Albums);
            album = stubAlbum.GetAlbumByName("Adios Bahamas");
            Assert.Contains(album, stubAlbum.Albums);
            collec = stubAlbum.GetAlbums();
            Assert.Equal(collec, stubAlbum.Albums);
        }
    }

}