using Model;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;

namespace TestUnitaires
{

    public class TU_Artist
    {
        [Theory]
        [InlineData("Hugo TSR")]
        [InlineData(null)]
        [InlineData("Hugo TSRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR")]
        public void TU_Attributes(string name)
        {
            Artist a = new Artist(name);
            Assert.True(a.Name != null && a.Name.Length < 75);
        }
    }

}