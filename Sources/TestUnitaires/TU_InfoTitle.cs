using Model;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;

namespace TestUnitaires
{

    public class TU_InfoTitle
    {
        [Theory]
        [InlineData("Trajectoire","morceau1.png","Sortie : 2020","Morceau de N�pal",Genre.HIP_HOP, 0)]
        [InlineData(null, "morceau1.png", "Sortie : 2020", "Morceau de N�pal", Genre.HIP_HOP, 0)]
        [InlineData("Trajectoire", null, "Sortie : 2020", "Morceau de N�pal", Genre.HIP_HOP, 1)]
        [InlineData("Trajectoire", "morceau1.png", null, "Morceau de N�pal", Genre.HIP_HOP, 1)]
        [InlineData("Trajectoire", "morceau1png", "Sortie : 2020", "Morceau de N�pal", Genre.HIP_HOP, 0)]
        [InlineData("Trajectoire", "morceau1. png", "Sortie : 2020", "Morceau de N�pal", Genre.HIP_HOP, 1)]
        public void TU_Attributes(string name, string url, string info, string desc, Genre g, long id)
        {
            InfoTitle it = new InfoTitle(name, url, info, desc, g, id);
            Assert.True(it.Name != null && it.Name.Length < 75);
            Assert.True(it.ImageURL != null && it.ImageURL.Contains('.'));
            Assert.True(it.Information != null && it.Information.Length < 500);
            Assert.True(it.Description != null && it.Description.Length < 500);
        }

        [Theory]
        [InlineData("Trajectoire", "morceau1.png", "Sortie : 2020", "Morceau de N�pal", Genre.HIP_HOP, 0)]
        [InlineData(null, "morceau1.png", "Sortie : 2020", "Morceau de N�pal", Genre.HIP_HOP, 0)]
        [InlineData("Trajectoire", null, "Sortie : 2020", "Morceau de N�pal", Genre.HIP_HOP, 1)]
        [InlineData("Trajectoire", "morceau1.png", null, "Morceau de N�pal", Genre.HIP_HOP, 1)]
        [InlineData("Trajectoire", "morceau1png", "Sortie : 2020", "Morceau de N�pal", Genre.HIP_HOP, 0)]
        [InlineData("Trajectoire", "morceau1. png", "Sortie : 2020", "Morceau de N�pal", Genre.HIP_HOP, 1)]
        public void TU_Methods(string name, string url, string info, string desc, Genre g, long id)
        {
            InfoTitle it = new InfoTitle(name, url, info, desc, g, id);
            Artist a = new Artist("Lahuiss");
            it.AddFeat(a);
            Assert.Contains(a, it.Feat);
            it.RemoveFeat(a);
            Assert.DoesNotContain(a, it.Feat);
        }
    }

}