﻿namespace TestUnitaires
{
    public class TU_Program
    {
        [Fact]
        public void TU_Methods()
        {
            List<Model.Artist> a = new() { new Model.Artist("artiste"), new Model.Artist("") };
            Console.ConsoleProgram cp = new();
            cp.FonctionalTests();
            cp.Next(1);
            cp.Previous(1);
            Console.ConsoleProgram.Separator();
            Console.ConsoleProgram.DisplaySerialization(a);
            Assert.NotNull(cp.Current);
        }
    }
}
