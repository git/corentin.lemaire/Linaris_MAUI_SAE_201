using Model;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;

namespace TestUnitaires
{

    public class TU_Title
    {
        [Theory]
        [InlineData("Trajectoire","morceau1.png","Sortie : 2020")]
        [InlineData(null, "morceau1.png", "Sortie : 2020")]
        [InlineData("Trajectoire", null, "Sortie : 2020")]
        [InlineData("Trajectoire", "morceau1.png", null)]
        [InlineData("Trajectoire", "morceau1png", "Sortie : 2020")]
        [InlineData("Trajectoire", "morceau1. png", "Sortie : 2020")]
        public void TU_Attributes(string name, string url, string info)
        {
            Title t = new Title(name, url, info);
            Assert.True(t.Name != null && t.Name.Length < 75);
            Assert.True(t.ImageURL != null && t.ImageURL.Contains('.'));
            Assert.True(t.Information != null && t.Information.Length < 500);
        }
    }

}