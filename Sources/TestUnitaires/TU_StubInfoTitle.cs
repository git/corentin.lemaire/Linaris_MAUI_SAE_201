using Model;
using Model.Stub;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;
using System.Collections.ObjectModel;

namespace TestUnitaires
{

    public class TU_StubInfoTitle
    {
        [Fact]
        public void TU_Methods()
        {
            StubInfoTitle sit = new StubInfoTitle();
            /*List<InfoTitle> list = new List<InfoTitle>();*/
            Artist artist = new Artist("Un mec");
            ObservableCollection<InfoTitle> collec = new ObservableCollection<InfoTitle>();
            InfoTitle info = new InfoTitle("Titre Sympa", "url.test", "infos", "desc", Genre.POP, 345);
            /*list = sit.GetInfoTitlesByNames(new List<string>{"Dégradation"});*/
            /*Assert.NotEmpty(list);*/
            sit.AddInfoTitle(info);
            Assert.Contains(info, sit.InfoTitles);
            sit.RemoveInfoTitle(info);
            Assert.DoesNotContain(info, sit.InfoTitles);
            collec = sit.GetInfoTitles();
            Assert.Equal(sit.InfoTitles, collec);
        }
    }

}