using Model;
using Model.Stub;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;

namespace TestUnitaires
{

    public class TU_Playlist
    {
        [Theory]
        [InlineData("Sons Soir�es","red-sky.png","Contient les sons que je mets quand je suis en soir�e.")]
        [InlineData(null, "red-sky.png", "Contient les sons que je mets quand je suis en soir�e.")]
        [InlineData("Sons Soir�es", null, "Contient les sons que je mets quand je suis en soir�e.")]
        [InlineData("Sons Soir�es", "red-sky.png", null)]
        [InlineData("Sons Soir�es", "redskypng", "Contient les sons que je mets quand je suis en soir�e.")]
        [InlineData("Sons Soir�es", "red-sky .png", "Contient les sons que je mets quand je suis en soir�e.")]
        public void TU_Attributes(string name, string url, string desc)
        {
            Playlist p = new Playlist(name, desc, url);
            Assert.True(p.Name != null && p.Name.Length < 75);
            Assert.True(p.ImageURL != null && p.ImageURL.Contains('.'));
            Assert.True(p.Description != null && p.Description.Length < 500);
            p.Index = 0;
            Assert.Equal(0, p.Index);
            p.Index = 1000;
            Assert.NotEqual(1000, p.Index);
            var list = p.Played;
            Assert.Equal(list, p.Played);
            p.IsSubMenuVisible = true;
            Assert.True(p.IsSubMenuVisible);
            var res = p.IsSubMenuVisible;
            Assert.Equal(p.IsSubMenuVisible, res);
            Playlist playlist = new();
            Assert.Equal(Manager.DEFAULT_NAME, playlist.Name);
            Assert.Equal(Manager.DEFAULT_DESC, playlist.Description);
            Assert.Equal(Manager.DEFAULT_URL, playlist.ImageURL);
        }
        [Theory]
        [InlineData("Sons Soir�es", "red-sky.png", "Contient les sons que je mets quand je suis en soir�e.")]
        [InlineData(null, "red-sky.png", "Contient les sons que je mets quand je suis en soir�e.")]
        [InlineData("Sons Soir�es", null, "Contient les sons que je mets quand je suis en soir�e.")]
        [InlineData("Sons Soir�es", "red-sky.png", null)]
        [InlineData("Sons Soir�es", "redskypng", "Contient les sons que je mets quand je suis en soir�e.")]
        [InlineData("Sons Soir�es", "red-sky .png", "Contient les sons que je mets quand je suis en soir�e.")]
        public void TU_Methods(string name, string url, string desc)
        {
            Playlist p = new(name, desc, url);
            CustomTitle t = new("D�bitage","test. mp3","Banger","path");

            p.AddTitle(t);
            Assert.Contains(t,p.Titles);
            p.RemoveTitle(t);
            Assert.DoesNotContain(t,p.Titles);

            var ct = p.NextTitle();
            var ct2 = p.PreviousTitle();
            Assert.Null(ct);
            Assert.Null(ct2);

            p.AddTitle(t);

            p.LoopTitle = true;
            ct = p.NextTitle();
            ct2 = p.PreviousTitle();
            Assert.Equal(t, ct);
            Assert.Equal(t, ct2);

            p.LoopTitle = false;
            ct = p.NextTitle();
            ct2 = p.PreviousTitle();
            Assert.Equal(t, ct);
            Assert.Equal(t, ct2);

            p.Shuffle = true;
            ct = p.NextTitle();
            ct2 = p.PreviousTitle();
            Assert.Equal(t, ct);
            Assert.Equal(t, ct2);
            ct2 = p.PreviousTitle();
            Assert.Equal(t, ct2);

            CustomTitle t2 = new("nom2", "url2.png", "info", "path2.mp3");
            CustomTitle t3 = new("nom3", "url3.png", "info", "path3.mp3");
            p.AddTitle(t2);
            p.AddTitle(t3);

            p.NextTitle();
            p.NextTitle();
            p.PreviousTitle();

            p.Index = -1;
            ct = p.GetCurrentTitle();
            Assert.NotNull(ct);

            p.Index = 1000;
            ct = p.GetCurrentTitle();

            p.GetHashCode();

            p.ToString();

            p.HasCustomTitle(ct);
            p.HasCustomTitle(new CustomTitle());

            var nb = Playlist.RandomGenerator(2);
            Assert.True(nb <= 2);
            Assert.True(nb > 0);
        }
    }

}