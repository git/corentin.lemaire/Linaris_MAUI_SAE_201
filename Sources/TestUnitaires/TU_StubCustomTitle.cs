using Model;
using Model.Stub;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;
using System.Collections.ObjectModel;

namespace TestUnitaires
{

    public class TU_StubCustomTitle
    {
        [Fact]
        public void TU_Methods()
        {
            StubCustomTitle sct = new StubCustomTitle();
            List<CustomTitle> list = new List<CustomTitle>();
            List<string> list2 = new List<string>() { "MaMusique" };
            Artist artist = new Artist("Un mec");
            ObservableCollection<CustomTitle> collec = new ObservableCollection<CustomTitle>();
            CustomTitle cust = new CustomTitle("Morceau de test", "mp3.pdf", "plein d'infos", "avec un petit chemin");
            list = sct.GetCustomTitlesByNames(list2).ToList();
            Assert.NotEmpty(list);
            sct.AddCustomTitle(cust);
            Assert.Contains(cust, sct.CustomTitles);
            sct.RemoveCustomTitle(cust);
            Assert.DoesNotContain(cust, sct.CustomTitles);
            collec = sct.GetCustomTitles();
            Assert.Equal(sct.CustomTitles, collec); 
        }
    }

}