using Model;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;

namespace TestUnitaires
{
    public class TU_Album
    {
        [Theory]
        [InlineData("Fen�tre sur Rue","album2. jpg","Un banger","Sortie : 2012")]
        [InlineData("Adios Bahamas", "album.jpg", "Un banger", "Sortie : 2012")]
        [InlineData(null, "album2.jpg", "Un banger", "Sortie : 2012")]
        [InlineData("Dans La L�gende", null, "Un banger", "Sortie : 2012")]
        [InlineData("Dans La L�gende","album1.jpg", null, "Sortie : 2012")]
        [InlineData("Dans La L�gende", "album1.jpg", "Un banger", null)]
        [InlineData("Dans La L�gende", "album1jpg", "Un banger", "Sortie : 2012")]
        public void TU_Attributes(string nameAlbum, string url, string desc, string info)
        {
            Album album = new Album(nameAlbum, url, new Artist("test"), desc, info);
            Assert.True(album.Name != null && album.Name.Length < 75);
            Assert.True(album.ImageURL != null && album.ImageURL.Contains('.'));
            Assert.False(album.ImageURL.Contains(' '));
            Assert.True(album.Description != null && album.Description.Length < 500);
            Assert.True(album.Information != null && album.Information.Length < 500);
        }

        [Theory]
        [InlineData("Fen�tre sur Rue", "album2. jpg", "Un banger", "Sortie : 2012")]
        [InlineData("Adios Bahamas", "album.jpg", "Un banger", "Sortie : 2012")]
        [InlineData(null, "album2.jpg", "Un banger", "Sortie : 2012")]
        [InlineData("Dans La L�gende", null, "Un banger", "Sortie : 2012")]
        [InlineData("Dans La L�gende", "album1.jpg", null, "Sortie : 2012")]
        [InlineData("Dans La L�gende", "album1.jpg", "Un banger", null)]
        [InlineData("Dans La L�gende", "album1jpg", "Un banger", "Sortie : 2012")]
        public void TU_Methods(string nameAlbum, string url, string desc, string info)
        {
            Album album = new Album(nameAlbum, url, new Artist("test"), desc, info);
            InfoTitle t = new InfoTitle("D�bitage", "test. mp3", "Banger", "", Genre.POP, 0);
            album.AddTitle(t);
            Assert.Contains(t, album.InfoTitles);
            album.RemoveTitle(t);
            Assert.DoesNotContain(t, album.InfoTitles);
        }

    }
}