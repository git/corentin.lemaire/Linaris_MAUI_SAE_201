using Model;
using Model.Stub;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;
using System.Collections.ObjectModel;

namespace TestUnitaires
{

    public class TU_StubPlaylist
    {
        [Fact]
        public void TU_Methods()
        {
            StubPlaylist stubPlaylist = new StubPlaylist();
            Playlist playlist = new Playlist("Une playlist", "Une description", "image.jpeg");
            ObservableCollection<Playlist> collec = new ObservableCollection<Playlist>();
            stubPlaylist.AddPlaylist(playlist);
            Assert.Contains(playlist, stubPlaylist.Playlists);
            stubPlaylist.RemovePlaylist(playlist);
            Assert.DoesNotContain(playlist, stubPlaylist.Playlists);
            playlist = stubPlaylist.GetPlaylistByName("Playlist1");
            Assert.Contains(playlist, stubPlaylist.Playlists);
            collec = stubPlaylist.GetPlaylists();
            Assert.Equal(collec, stubPlaylist.Playlists);
        }
    }

}