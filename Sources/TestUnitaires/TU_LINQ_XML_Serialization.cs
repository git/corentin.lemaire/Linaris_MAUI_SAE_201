using Model;
using Model.Serialization;
using Model.Stub;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;
using System;

namespace TestUnitaires
{

    public class TU_LINQ_XML_Serialization
    {
        [Theory]
        [InlineData("R�ference")]
        public void TU_Methods(string? test)
        {
            Playlist p = new(test, "PlaceHolder", "place.holder");
            Album album = new(test, "place.holder", new Artist("test"), "PlaceHolder", "PlaceHolder");
            CustomTitle t = new(test, "test. mp3", "Banger", "path");
            Artist artist = new(test);
            InfoTitle info = new(test, "url.test", "infos", "desc", Genre.POP, 345);
            LinqXmlSerialization l = new(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data"));
            Assert.NotEmpty(l.Albums);
            Assert.NotEmpty(l.Artists);
            Assert.NotEmpty(l.InfoTitles);
            Assert.Empty(l.Playlists);
            Assert.Empty(l.CustomTitles);
            l.AddPlaylist(p);
            l.AddAlbum(album);
            l.AddCustomTitle(t);
            l.AddArtist(artist);
            l.AddInfoTitle(info);
            Assert.Contains(t, l.CustomTitles);
            Assert.Contains(album, l.Albums);
            Assert.Contains(p, l.Playlists);
            Assert.Contains(info, l.InfoTitles);
            Assert.Contains(artist, l.Artists);
            l.RemovePlaylist(p);
            l.RemoveAlbum(album);
            l.RemoveCustomTitle(t);
            l.RemoveInfoTitle(info);
            Assert.DoesNotContain(t, l.CustomTitles);
            Assert.DoesNotContain(p, l.Playlists);
            Assert.DoesNotContain(album, l.Albums);
            Assert.DoesNotContain(info,l.InfoTitles);
            l.AddCustomTitle(t);
            l.AddPlaylist(p);
            l.AddAlbum(album);
            l.AddArtist(artist);
            l.AddInfoTitle(info);
            Assert.Equal(l.GetAlbums(), l.Albums);
            Assert.Equal(l.GetInfoTitles(), l.InfoTitles);
            Assert.Equal(l.GetCustomTitles(), l.CustomTitles);
            Assert.Equal(l.GetPlaylists(), l.Playlists);
            Assert.Equal(l.GetArtists(), l.Artists);
            Assert.Equal(l.GetAlbumByName(test), album);
            Assert.Equal(l.GetPlaylistByName(test), p);
            Assert.Equal(l.GetInfoTitleByName(test), info);
            Assert.Equal(l.GetArtistByName(test), artist);
        }

        [Theory]
        [InlineData("Imagine Dragons")]
        [InlineData("Nepal")]
        [InlineData("Booba")]
        public void TU_ExistsArtist_Method(string name)
        {
            LinqXmlSerialization l = new(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data"));
            Assert.True(l.ExistsArtist(new Artist(name)));
            Assert.True(l.ExistsArtistByName(name));
        }

        [Theory]
        [InlineData("Fenetre Sur Rue", "album2.jpg", "Hugo TSR", "Un banger", "Sortie : 2012")]
        [InlineData("Adios Bahamas", "album1.jpg", "Nepal", "Album post-mortem qui sign� �galement le dernier de l'artiste", "Sortie : 2020")]
        public void TU_ExistsAlbum_Method(string name, string imageURL, string artistName, string desc, string info)
        {
            LinqXmlSerialization l = new(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data"));
            Assert.True(l.ExistsAlbum(new Album(name, imageURL, new Artist(artistName), desc, info)));
            Assert.True(l.ExistsAlbumByName(name));
        }

        [Theory]
        [InlineData("Trajectoire", "morceau1.png", "Sortie : 2020", "Morceau de N�pal", Genre.HIP_HOP, 0)]
        [InlineData("Bones", "album14.jpg", "Titre de l'album Mercury Act 2", "La chanson \"Bones\" a �t� publi�e en tant que premier single de Mercury - Act 2 le 11 mars 2022. La chanson a �t� utilis�e pour promouvoir la troisi�me saison de la s�rie Amazon Prime Video The Boys. La sortie du clip de la chanson le 6 avril a co�ncid� avec la pr�commande de l'album.", Genre.POP, 13)]
        public void TU_ExistsInfoTitles_Method(string name, string imageURL, string desc, string info, Genre genre, long id)
        {
            LinqXmlSerialization l = new(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data"));
            Assert.True(l.ExistsInfoTitle(new InfoTitle(name, imageURL, desc, info, genre, id)));
            Assert.True(l.ExistsInfoTitleByName(name));
        }

        [Theory]
        [InlineData("Trajectoire", "morceau1.png", "Sortie : 2020")]
        [InlineData("Bones", "album14.jpg", "Titre de l'album Mercury Act 2")]
        public void TU_ExistsPlaylist_Method(string name, string imageURL, string desc)
        {
            LinqXmlSerialization l = new(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data"));
            Assert.False(l.ExistsPlaylist(new Playlist(name, desc, imageURL)));
            Assert.False(l.ExistsPlaylistByName(name));
        }

        [Theory]
        [InlineData("Trajectoire", "morceau1.png", "Sortie : 2020", "path.mp3")]
        [InlineData("Bones", "album14.jpg", "Titre de l'album Mercury Act 2", "path.mp3")]
        public void TU_ExistsCustomTitle_Method(string name, string imageURL, string info, string path)
        {
            LinqXmlSerialization l = new(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data"));
            Assert.False(l.ExistsCustomTitle(new CustomTitle(name, imageURL, info, path)));
            Assert.False(l.ExistsCustomTitleByName(name));
        }

        [Fact]
        public void TU_RemoveAddMultiple()
        {
            List<Album> albums = new()
            {
                new Album("album1", "img1.png", new Artist("artist"), "desc", "info"),
                new Album("album2", "img2.png", new Artist("artist"), "desc", "info"),
                new Album("album3", "img3.png", new Artist("artist"), "desc", "info")
            };
            List<Artist> artists = new()
            {
                new Artist("artist1"),
                new Artist("artist2"),
                new Artist("artist3")
            };
            List<InfoTitle> infoTitles = new()
            {
                new InfoTitle("it1", "img1.png", "desc", "info", Genre.POP, 0),
                new InfoTitle("it2", "img2.png", "desc", "info", Genre.POP, 0),
                new InfoTitle("it3", "img3.png", "desc", "info", Genre.POP, 0)
            };
            List<CustomTitle> customTitles = new()
            {
                new CustomTitle("ct1", "img1.png", "info", "path1.mp3"),
                new CustomTitle("ct2", "img2.png", "info", "path2.mp3"),
                new CustomTitle("ct3", "img3.png", "info", "path3.mp3")
            };
            List<Playlist> playlists = new()
            {
                new Playlist("nom1", "desc", "img1.png"),
                new Playlist("nom2", "desc", "img2.png"),
                new Playlist("nom3", "desc", "img3.png")
            };
            LinqXmlSerialization l = new(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data"));
            l.AddAlbums(albums);
            l.AddArtists(artists);
            l.AddInfoTitles(infoTitles);
            l.AddCustomTitles(customTitles);
            l.AddPlaylists(playlists);

            l.RemoveAlbums(albums);
            l.RemoveArtists(artists);
            l.RemoveInfoTitles(infoTitles);
            l.RemoveCustomTitles(customTitles);
            l.RemovePlaylists(playlists);

            l.AddAlbums(albums);
            l.AddArtists(artists);
            l.AddInfoTitles(infoTitles);
            l.AddCustomTitles(customTitles);
            l.AddPlaylists(playlists);
        }

        [Theory]
        [InlineData(Genre.ROCK, "ROCK")]
        [InlineData(Genre.POP, "POP")]
        [InlineData(Genre.K_POP, "K_POP")]
        [InlineData(Genre.HIP_HOP, "HIP_HOP")]
        [InlineData(Genre.CLASSIQUE, "CLASSIQUE")]
        [InlineData(Genre.VARIETE_FRANCAISE, "VARIETE_FRANCAISE")]
        [InlineData(Genre.VARIETE_INTERNATIONALE, "VARIETE_INTERNATIONALE")]
        [InlineData(Genre.RAP, "RAP")]
        [InlineData(Genre.BLUES, "BLUES")]
        [InlineData(Genre.ELECTRO, "ELECTRO")]
        [InlineData(Genre.COUNTRY, "COUNTRY")]
        [InlineData(Genre.DISCO, "DISCO")]
        [InlineData(Genre.METAL, "METAL")]
        [InlineData(Genre.FUNK, "FUNK")]
        [InlineData(Genre.GOSPEL, "GOSPEL")]
        [InlineData(Genre.RNB, "RNB")]
        [InlineData(Genre.REGGAE, "REGGAE")]
        [InlineData(Genre.JAZZ, "JAZZ")]
        public void TU_Genre(Genre genre, string genreString)
        {
            Assert.Equal(genre, LinqXmlSerialization.GetGenreByName(genreString));
        }

        [Fact]
        public void TU_SaveLoad()
        {
            LinqXmlSerialization l = new(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data"));
            LinqXmlSerialization.SaveAlbums();
            LinqXmlSerialization.SaveInfoTitles();
            LinqXmlSerialization.SaveArtists();
            l.SavePlaylists();
            l.SaveCustomTitles();
            l.SaveSerialization();
            l.LoadSerialization();

            l.LoadAlbums();
            l.LoadArtists();
            l.LoadInfoTitles();
            l.LoadCustomTitles();
            l.LoadPlaylists();
            l.LoadSerialization();

            Assert.NotEmpty(l.Albums);
            Assert.NotEmpty(l.Artists);
            Assert.NotEmpty(l.InfoTitles);
            Assert.Empty(l.CustomTitles);
            Assert.Empty(l.Playlists);

        }
    }
}