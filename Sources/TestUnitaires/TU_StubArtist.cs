using Model;
using Model.Stub;
using Newtonsoft.Json.Linq;
using NuGet.Frameworks;

namespace TestUnitaires
{

    public class TU_StubArtist
    {
        List<Artist> test = new();

        [Fact]
        public void TU_Attributes()
        {
            StubArtist stubArtist = new StubArtist();
            List<Artist> artists = new List<Artist>() { new Artist("Imagine Dragons"), new Artist("Nepal"), new Artist("Hugo TSR"), new Artist("Booba"), new Artist("Oxmo Puccino"), new Artist("IAM"), new Artist("PNL") };
            Assert.Equal(stubArtist.Artists, artists);
        }

        [Fact]
        public void TU_Methods()
        {
            StubArtist stubArtist = new StubArtist();
            List<Artist> list = new List<Artist>();
            Artist artist = new Artist("Un mec");
            Assert.Empty(list);
            stubArtist.AddArtist(artist);
            Assert.Contains(artist, stubArtist.Artists);
            stubArtist.RemoveArtist(artist);
            Assert.DoesNotContain(artist, stubArtist.Artists);
            artist = stubArtist.GetArtistByName("Nepal");
            stubArtist.AddArtist(artist);
            Assert.Contains(artist,stubArtist.Artists);
            list = stubArtist.GetArtists();
            Assert.Equal(list, stubArtist.Artists);
        }
    }

}