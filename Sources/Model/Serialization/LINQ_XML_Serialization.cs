﻿using System.Xml;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using Model.Stub;
using Microsoft.Maui.Devices;

namespace Model.Serialization
{
    public class LinqXmlSerialization : IDataManager
    {
        private readonly string XMLFILEPLAYLISTS;

        private readonly string XMLFILECUSTOMS;

        private readonly StubInfoTitle stubInfoTitle = new();

        public StubInfoTitle StubInfoTitle
        {
            get => stubInfoTitle;
        }

        private List<Artist> artists;

        public IEnumerable<Artist> Artists
        {
            get
            {
                return new List<Artist>(artists);
            }
        }

        private ObservableCollection<Album> albums;

        public ObservableCollection<Album> Albums
        {
            get
            {
                return new ObservableCollection<Album>(albums);
            }
        }

        private ObservableCollection<Playlist> playlists;

        public ObservableCollection<Playlist> Playlists
        {
            get
            {
                return new ObservableCollection<Playlist>(playlists);
            }
        }

        private ObservableCollection<InfoTitle> infoTitles;

        public ObservableCollection<InfoTitle> InfoTitles
        {
            get
            {
                return new ObservableCollection<InfoTitle>(infoTitles);
            }
        }

        private ObservableCollection<CustomTitle> customTitles;

        public ObservableCollection<CustomTitle> CustomTitles
        {
            get
            {
                return new ObservableCollection<CustomTitle>(customTitles);
            }
        }

        /// <summary>
        /// Constructeur de la classe LinqXmlSerialization
        /// </summary>
        /// <param name="pathDirectory">Chemin d'accès des fichiers de sauvegarde des données</param>
        public LinqXmlSerialization(string pathDirectory)
        {
            string XMLPATH = pathDirectory;
            XMLFILECUSTOMS = Path.Combine(XMLPATH, "playlists.xml");
            XMLFILEPLAYLISTS = Path.Combine(XMLPATH, "customs.xml");
            playlists = new ObservableCollection<Playlist>();
            artists = new List<Artist>();
            infoTitles = new ObservableCollection<InfoTitle>();
            albums = new ObservableCollection<Album>();
            customTitles = new ObservableCollection<CustomTitle>();
            if (!Directory.Exists(XMLPATH))
            {
                Directory.CreateDirectory(XMLPATH);
            }
            Directory.SetCurrentDirectory(XMLPATH);
            LoadSerialization();
        }


        /// <summary>
        /// Permet d'ajouter un objet Album à l'application
        /// </summary>
        /// <param name="album">Album à ajouter</param>
        public void AddAlbum(Album album)
        {
            albums.Add(album);
        }

        /// <summary>
        /// Permet d'ajouter un objet Artist à l'application
        /// </summary>
        /// <param name="artist">Artist à ajouter</param>
        public void AddArtist(Artist artist)
        {
            artists.Add(artist);
        }

        /// <summary>
        /// Permet d'ajouter un objet CustomTitle à l'application
        /// </summary>
        /// <param name="title">CustomTitle à ajouter</param>
        public void AddCustomTitle(CustomTitle title)
        {
            customTitles.Add(title);
        }

        /// <summary>
        /// Permet d'ajouter un objet InfoTitle à l'application
        /// </summary>
        /// <param name="title">InfoTitle à ajouter</param>
        public void AddInfoTitle(InfoTitle title)
        {
            infoTitles.Add(title);
        }

        /// <summary>
        /// Permet d'ajouter un objet Playlist à l'application
        /// </summary>
        /// <param name="playlist">Playlist à ajouter</param>
        public void AddPlaylist(Playlist playlist)
        {
            playlists.Add(playlist);
        }

        /// <summary>
        /// Permet d'obtenir une liste d'objets Album
        /// </summary>
        /// <returns>Retourne une liste d'objets Album</returns>
        public ObservableCollection<Album> GetAlbums()
        {
            return albums;
        }

        /// <summary>
        /// Permet d'obtenir une liste d'objets Artist
        /// </summary>
        /// <returns>Retourne une liste d'objets Artist</returns>
        public List<Artist> GetArtists()
        {
            return artists;
        }

        /// <summary>
        /// Permet d'obtenir une liste d'objets CustomTitle
        /// </summary>
        /// <returns>Retourne une liste d'objets CustomTitle</returns>
        public ObservableCollection<CustomTitle> GetCustomTitles()
        {
            return customTitles;
        }

        /// <summary>
        /// Permet d'obtenir une liste d'objets InfoTitle
        /// </summary>
        /// <returns>Retourne une liste d'objets InfoTitle</returns>
        public ObservableCollection<InfoTitle> GetInfoTitles()
        {
            return infoTitles;
        }

        /// <summary>
        /// Permet d'obtenir une liste d'objets Playlist
        /// </summary>
        /// <returns>Retourne une liste d'objets Playlist</returns>
        public ObservableCollection<Playlist> GetPlaylists()
        {
            return playlists;
        }

        /// <summary>
        /// Permet de retirer un objet Album de l'application
        /// </summary>
        /// <param name="album">Album à retirer</param>
        public void RemoveAlbum(Album album)
        {
            albums.Remove(album);
        }

        /// <summary>
        /// Permet de retirer un objet Artist de l'application
        /// </summary>
        /// <param name="artist">Artist à retirer</param>
        public void RemoveArtist(Artist artist)
        {
            artists.Remove(artist);
        }

        /// <summary>
        /// Permet de retirer un objet CustomTitle de l'application
        /// </summary>
        /// <param name="title">CustomTitle à retirer</param>
        public void RemoveCustomTitle(CustomTitle title)
        {
            customTitles.Remove(title);
        }

        /// <summary>
        /// Permet de retirer un objet InfoTitle de l'application
        /// </summary>
        /// <param name="title">InfoTitle à retirer</param>
        public void RemoveInfoTitle(InfoTitle title)
        {
            infoTitles.Remove(title);
        }

        /// <summary>
        /// Permet de retirer un objet Playlist de l'application
        /// </summary>
        /// <param name="playlist">Playlist à retirer</param>
        public void RemovePlaylist(Playlist playlist)
        {
            playlists.Remove(playlist);
        }

        /// <summary>
        /// Permet de charger les données
        /// </summary>
        public void LoadSerialization()
        {
            LoadArtists();
            LoadAlbums();
            LoadInfoTitles();
            LoadCustomTitles();
            LoadPlaylists();
        }

        /// <summary>
        /// Permet de sauvegarder l'état de l'application
        /// </summary>
        public void SaveSerialization()
        {
            SaveArtists();
            SaveCustomTitles();
            SaveInfoTitles();
            SavePlaylists();
            SaveAlbums();
        }

        /// <summary>
        /// Permet de charger les playlists depuis le fichier correspondant
        /// </summary>
        public void LoadPlaylists()
        {
            if (!File.Exists(XMLFILEPLAYLISTS))
            {
                SavePlaylists();
            }
            XDocument PlaylistsFichier = XDocument.Load(XMLFILEPLAYLISTS);

            var playlists2 = PlaylistsFichier.Descendants("Playlist")
                                        .Select(eltPlaylist => new Playlist(
                                            eltPlaylist.Attribute("Name")!.Value,
                                            eltPlaylist.Element("Description")!.Value,
                                            eltPlaylist.Element("ImageURL")!.Value
                                        )).ToList();
            playlists = new ObservableCollection<Playlist>(playlists2);

            foreach (Playlist playlist in playlists)
            {
                var allCustoms = PlaylistsFichier.Descendants("Playlist")
                                                     .Single(ct => ct.Attribute("Name")?.Value == playlist.Name)
                                                     .Element("Titles")!.Value;

                if (allCustoms == null)
                {
                    continue;
                }

                char[] separator = new char[] { ';' };
                string[] customArray = allCustoms.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                List<string> customsList = customArray.ToList();

                foreach (var custom in customsList)
                {
                    CustomTitle customTitle = GetCustomTitleByPath(custom);

                    if (customTitle == null)
                    {
                        continue;
                    }

                    playlist.AddTitle(customTitle);
                }
            }
        }
        /// <summary>
        /// Permet de sauvegarder les playlists dans le fichier correpondant 
        /// </summary>
        public void SavePlaylists()
        {
            XDocument PlaylistsFichier = new XDocument();

            var playlist = playlists.Select(playlist => new XElement("Playlist",
                                                        new XAttribute("Name", playlist.Name),
                                                        new XElement("Description", playlist.Description),
                                                        new XElement("ImageURL", playlist.ImageURL),
                                                        new XElement("Titles", playlist.Titles.Any() ? playlist.Titles.Select(p => p.Path).Aggregate((playlistName, nextPlaylist) => playlistName + ";" + nextPlaylist) : "")
                                                        ));
            PlaylistsFichier.Add(new XElement("Playlists", playlist));

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            using (TextWriter tw = File.CreateText(XMLFILEPLAYLISTS))
            {
                using (XmlWriter writer = XmlWriter.Create(tw, settings))
                {
                    PlaylistsFichier.Save(writer);
                }
            }
        }

        /// <summary>
        /// Permet de charger les artistes depuis les stubs
        /// </summary>
        public void LoadArtists()
        {
            artists = StubInfoTitle.StubAlbum.StubArtist.GetArtists();
        }

        /// <summary>
        /// Permet de sauvegarder les artistes. Ne fais rien car ce sont les stubs qui s'en occupent
        /// </summary>
        public static void SaveArtists()
        {
            // Don't do anything because it's static data
        }

        /// <summary>
        /// Permet de charger les CustomTitles depuis le fichier de sauvegarde correspondant
        /// </summary>
        public void LoadCustomTitles()
        {
            if (!File.Exists(XMLFILECUSTOMS))
            {

                XDocument CustomsFile2 = new();

                var custom = customTitles.Select(c => new XElement("CustomTitle",
                                                      new XAttribute("Name", c.Name),
                                                      new XElement("ImageURL", c.ImageURL),
                                                      new XElement("Information", c.Information),
                                                      new XElement("Path", c.Path)
                                                      ));

                CustomsFile2.Add(new XElement("Customs", custom));

                XmlWriterSettings settings = new()
                {
                    Indent = true
                };

                using TextWriter tw = File.CreateText(XMLFILECUSTOMS);
                using XmlWriter writer = XmlWriter.Create(tw, settings);
                        CustomsFile2.Save(writer);
            }
            XDocument CustomsFile = XDocument.Load(XMLFILECUSTOMS);

            var customTitles2 = CustomsFile.Descendants("CustomTitle")
                                        .Select(eltPlaylist => new CustomTitle(
                                            eltPlaylist.Attribute("Name")!.Value,
                                            eltPlaylist.Element("ImageURL")!.Value,
                                            eltPlaylist.Element("Information")!.Value,
                                            eltPlaylist.Element("Path")!.Value
                                        )).ToList();
            customTitles = new ObservableCollection<CustomTitle>(customTitles2);
        }

        /// <summary>
        /// Permet de sauvegarder les CustomTitles dans le fichier correspondant
        /// </summary>
        public void SaveCustomTitles()
        {
            XDocument CustomsFile = new XDocument();

            var customs = customTitles.Select(custom => new XElement("CustomTitle",
                                                        new XAttribute("Name", custom.Name),
                                                        new XElement("ImageURL", custom.ImageURL),
                                                        new XElement("Information", custom.Information),
                                                        new XElement("Path", custom.Path)
                                                        ));

            CustomsFile.Add(new XElement("Customs", customs));

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            using (TextWriter tw = File.CreateText(XMLFILECUSTOMS))
            {
                using (XmlWriter writer = XmlWriter.Create(tw, settings))
                {
                    CustomsFile.Save(writer);
                }
            }
        }

        /// <summary>
        /// Permet de charger les albums depuis les stubs
        /// </summary>
        public void LoadAlbums()
        {
            albums = StubInfoTitle.StubAlbum.GetAlbums();
        }

        /// <summary>
        /// Permet de sauvegarder les albums. Ne fais rien car ce sont les stubs qui s'en occupent
        /// </summary>
        public static void SaveAlbums()
        {
            // Don't do anything because it's static data
        }

        /// <summary>
        /// Permet de charger les InfoTitles depuis les stubs
        /// </summary>
        public void LoadInfoTitles()
        {
            infoTitles = StubInfoTitle.GetInfoTitles();
            foreach (var infoTitle in InfoTitles)
            {
                GetAlbumById(infoTitle.AlbumID)?.AddTitle(infoTitle);
            }
        }

        /// <summary>
        /// Permet de sauvegarder les InfoTitles. Ne fais rien car ce sont les stubs qui s'en occupent
        /// </summary>
        public static void SaveInfoTitles()
        {
            // Don't do anything because it's static data
        }

        /// <summary>
        /// Permet de passer d'un genre à son nom en chaîne de caractère
        /// </summary>
        /// <param name="genre">Genre à transformer</param>
        /// <returns>Retourne la chaîne de caractère correspondant au genre</returns>
        public static Genre GetGenreByName(string genre)
        {
            if (genre == "HIP_HOP") return Genre.HIP_HOP;
            if (genre == "POP") return Genre.POP;
            if (genre == "ROCK") return Genre.ROCK;
            if (genre == "ELECTRO") return Genre.ELECTRO;
            if (genre == "CLASSIQUE") return Genre.CLASSIQUE;
            if (genre == "JAZZ") return Genre.JAZZ;
            if (genre == "VARIETE_FRANCAISE") return Genre.VARIETE_FRANCAISE;
            if (genre == "VARIETE_INTERNATIONALE") return Genre.VARIETE_INTERNATIONALE;
            if (genre == "REGGAE") return Genre.REGGAE;
            if (genre == "RAP") return Genre.RAP;
            if (genre == "RNB") return Genre.RNB;
            if (genre == "DISCO") return Genre.DISCO;
            if (genre == "BLUES") return Genre.BLUES;
            if (genre == "COUNTRY") return Genre.COUNTRY;
            if (genre == "FUNK") return Genre.FUNK;
            if (genre == "GOSPEL") return Genre.GOSPEL;
            if (genre == "METAL") return Genre.METAL;
            else return Genre.K_POP;
        }

        /// <summary>
        /// Permet d'obtenir un objet InfoTitle à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet InfoTitle</param>
        /// <returns>Retourne l'objet InfoTitle avec un nom équivalent à celui donné en paramètre</returns>
        public InfoTitle GetInfoTitleByName(string name)
        {
            return infoTitles.FirstOrDefault(it => it.Name.Equals(name));
        }

        /// <summary>
        /// Permet d'obtenir un objet Artist à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Artist</param>
        /// <returns>Retourne l'objet Artist avec un nom équivalent à celui donné en paramètre</returns>
        public Artist GetArtistByName(string name)
        {
            return artists.FirstOrDefault(artist => artist.Name.Equals(name));
        }

        /// <summary>
        /// Permet d'obtenir un objet Album à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Album</param>
        /// <returns>Retourne l'objet Album avec un nom équivalent à celui donné en paramètre</returns>
        public Album GetAlbumByName(string name)
        {
            return albums.FirstOrDefault(a => a.Name.Equals(name));
        }

        /// <summary>
        /// Permet d'obtenir un objet Album à partir de son ID
        /// </summary>
        /// <param name="id">ID de l'objet Album</param>
        /// <returns>Retourne l'objet Album avec un ID équivalent à celui donné en paramètre</returns>
        public Album GetAlbumById(long id)
        {
            return albums.FirstOrDefault(a => a.ID == id);
        }

        /// <summary>
        /// Permet d'obtenir un objet CustomTitle à partir de son chemin d'accès
        /// </summary>
        /// <param name="custom">Chemin d'accès de l'objet CustomTitle</param>
        /// <returns>Retourne l'objet CustomTitle avec un chemin d'accès équivalent à celui donné en paramètre</returns>
        public CustomTitle GetCustomTitleByPath(string custom)
        {
            return customTitles.FirstOrDefault(customTitle => customTitle.Path.Equals(custom));
        }

        /// <summary>
        /// Permet d'ajouter des objets Album à l'application
        /// </summary>
        /// <param name="albumsList">Liste d'Album à ajouter</param>
        public void AddAlbums(List<Album> albumsList)
        {
            foreach (Album a in albumsList)
            {
                albums.Add(a);
            }
        }

        /// <summary>
        /// Permet d'ajouter des objets Artist à l'application
        /// </summary>
        /// <param name="artistsList">Liste d'Artist à ajouter</param>
        public void AddArtists(List<Artist> artistsList)
        {
            foreach (Artist a in artistsList)
            {
                artists.Add(a);
            }
        }

        /// <summary>
        /// Permet d'ajouter des objets Playlist à l'application
        /// </summary>
        /// <param name="playlistsList">Liste de Playlist à ajouter</param>
        public void AddPlaylists(List<Playlist> playlistsList)
        {
            foreach (Playlist p in playlistsList)
            {
                playlists.Add(p);
            }
        }

        /// <summary>
        /// Permet d'ajouter des objets CustomTitle à l'application
        /// </summary>
        /// <param name="customTitlesList">Liste de CustomTitle à ajouter</param>
        public void AddCustomTitles(List<CustomTitle> customTitlesList)
        {
            foreach (CustomTitle ct in customTitlesList)
            {
                customTitles.Add(ct);
            }
        }

        /// <summary>
        /// Permet d'ajouter des objets InfoTitle à l'application
        /// </summary>
        /// <param name="infoTitlesList">Liste d'InfoTitle à ajouter</param>
        public void AddInfoTitles(List<InfoTitle> infoTitlesList)
        {
            foreach (InfoTitle it in infoTitlesList)
            {
                infoTitles.Add(it);
            }
        }

        /// <summary>
        /// Permet d'obtenir un objet Playlist à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Playlist</param>
        /// <returns>Retourne l'objet Playlist avec un nom équivalent à celui donné en paramètre</returns>
        public Playlist GetPlaylistByName(string name)
        {
            return playlists.FirstOrDefault(p => p.Name.Equals(name));
        }

        /// <summary>
        /// Permet de retirer des objets Album de l'application
        /// </summary>
        /// <param name="albumsList">Liste des objets Album à retirer de l'application</param>
        public void RemoveAlbums(List<Album> albumsList)
        {
            foreach (Album album in albumsList)
            {
                albums.Remove(album);
            }
        }

        /// <summary>
        /// Permet de retirer des objets Artist de l'application
        /// </summary>
        /// <param name="artistsList">Liste des objets Artist à retirer de l'application</param>
        public void RemoveArtists(List<Artist> artistsList)
        {
            foreach (Artist artist in artistsList)
            {
                artists.Remove(artist);
            }
        }

        /// <summary>
        /// Permet de retirer des objets Playlist de l'application
        /// </summary>
        /// <param name="playlistsList">Liste des objets Playlist à retirer de l'application</param>
        public void RemovePlaylists(List<Playlist> playlistsList)
        {
            foreach (Playlist playlist in playlistsList)
            {
                playlists.Remove(playlist);
            }
        }

        /// <summary>
        /// Permet de retirer des objets CustomTitle de l'application
        /// </summary>
        /// <param name="customTitlesList">Liste des objets CustomTitle à retirer de l'application</param>
        public void RemoveCustomTitles(List<CustomTitle> customTitlesList)
        {
            foreach (CustomTitle customTitle in customTitlesList)
            {
                customTitles.Remove(customTitle);
            }
        }

        /// <summary>
        /// Permet de retirer des objets InfoTitle de l'application
        /// </summary>
        /// <param name="infoTitlesList">Liste des objets InfoTitle à retirer de l'application</param>
        public void RemoveInfoTitles(List<InfoTitle> infoTitlesList)
        {
            foreach (InfoTitle infoTitle in infoTitlesList)
            {
                infoTitles.Remove(infoTitle);
            }
        }

        /// <summary>
        /// Permet de savoir si un objet Playlist appartient ou non à l'application
        /// </summary>
        /// <param name="playlist">Playlist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsPlaylist(Playlist playlist)
        {
            return playlists.Any(p => p.Equals(playlist));
        }

        /// <summary>
        /// Permet de savoir si un objet Playlist appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Playlist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsPlaylistByName(string name)
        {
            return playlists.Any(p => p.Name.Equals(name));
        }

        /// <summary>
        /// Permet de savoir si un objet Album appartient ou non à l'application
        /// </summary>
        /// <param name="album">Album à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsAlbum(Album album)
        {
            return albums.Any(a => a.Equals(album));
        }

        /// <summary>
        /// Permet de savoir si un objet Album appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Album à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsAlbumByName(string name)
        {
            return albums.Any(a => a.Name.Equals(name));
        }

        /// <summary>
        /// Permet de savoir si un objet Artist appartient ou non à l'application
        /// </summary>
        /// <param name="artist">Artist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsArtist(Artist artist)
        {
            return artists.Any(a => a.Equals(artist));
        }

        /// <summary>
        /// Permet de savoir si un objet Artist appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Artist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsArtistByName(string name)
        {
            return artists.Any(a => a.Name.Equals(name));
        }

        /// <summary>
        /// Permet de savoir si un objet CustomTitle appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="title">Nom de l'objet CustomTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsCustomTitle(CustomTitle title)
        {
            return customTitles.Any(ct => ct.Equals(title));
        }

        /// <summary>
        /// Permet de savoir si un objet CustomTitle appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet CustomTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsCustomTitleByName(string name)
        {
            return customTitles.Any(ct => ct.Name.Equals(name));
        }

        /// <summary>
        /// Permet de savoir si un objet InfoTitle appartient ou non à l'application
        /// </summary>
        /// <param name="title">InfoTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsInfoTitle(InfoTitle title)
        {
            return infoTitles.Any(it => it.Equals(title));
        }

        /// <summary>
        /// Permet de savoir si un objet InfoTitle appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet InfoTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsInfoTitleByName(string name)
        {
            return infoTitles.Any(it => it.Name.Equals(name));
        }
    }
}