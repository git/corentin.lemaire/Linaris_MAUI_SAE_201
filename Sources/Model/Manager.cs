﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Model.Stub
{
    /// <remarks>
    /// Classe Manager
    /// </remarks>
    public class Manager : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public readonly static int MAX_NAME_LENGTH = 75;

        public readonly static int MAX_DESCRIPTION_LENGTH = 500;

        public readonly static string DEFAULT_NAME = "Unknown";

        public readonly static string DEFAULT_URL = "none.png";

        public readonly static string DEFAULT_DESC = "";

        public IDataManager DataManager { get; set; }

        private ObservableCollection<Album> albums;

        public ObservableCollection<Album> Albums
        {
            get
            {
                return new ObservableCollection<Album>(albums);
            }
        }

        private ObservableCollection<CustomTitle> customTitles;

        public ObservableCollection<CustomTitle> CustomTitles
        {
            get
            {
                return new ObservableCollection<CustomTitle>(customTitles);
            }
        }

        private ObservableCollection<InfoTitle> infoTitles;

        public ObservableCollection<InfoTitle> InfoTitles
        {
            get
            {
                return new ObservableCollection<InfoTitle>(infoTitles);
            }
        }

        private ObservableCollection<Playlist> playlists;

        public ObservableCollection<Playlist> Playlists
        {
            get
            {
                return new ObservableCollection<Playlist>(playlists);
            }
        }

        private List<Artist> artists;

        public IEnumerable<Artist> Artists
        {
            get
            {
                return new List<Artist>(artists);
            }
        }

        private Album currentAlbum;

        public Album CurrentAlbum
        {
            get => currentAlbum;
            set
            {
                currentAlbum = value;
                OnPropertyChanged();
            }
        }

        private Playlist currentPlaylist;

        public Playlist CurrentPlaylist
        {
            get => currentPlaylist;
            set
            {
                currentPlaylist = value;
                OnPropertyChanged();
            }
        }

        private InfoTitle currentInfoTitle;

        public InfoTitle CurrentInfoTitle
        {
            get => currentInfoTitle;
            set
            {
                currentInfoTitle = value;
                OnPropertyChanged();
            }
        }

        private CustomTitle currentPlaying;

        public CustomTitle CurrentPlaying
        {
            get => currentPlaying;
            set
            {
                currentPlaying = value;
                OnPropertyChanged();
            }
        }

        public Dictionary<string, IEnumerable<Album>> AlbumsFromArtist { get; set; }

        public Dictionary<string, IEnumerable<InfoTitle>> InfoTitlesFromArtist { get; set; }

        /// <summary>
        /// Constructeur de la classe Manager
        /// </summary>
        /// <param name="dataManager">Méthode de sérialisation, gestionnaire des données</param>
        public Manager(IDataManager dataManager)
        {
            DataManager = dataManager;

            albums = DataManager.GetAlbums();
            customTitles = DataManager.GetCustomTitles();
            infoTitles = DataManager.GetInfoTitles();
            playlists = DataManager.GetPlaylists();
            artists = DataManager.GetArtists();

            currentAlbum = albums.First();
            currentPlaylist = playlists.FirstOrDefault();
            currentInfoTitle = null;
            currentPlaying = null;

            LoadDictionaries();
        }

        /// <summary>
        /// Remplis les dictionnaire grâce aux stubs et à LINQ
        /// </summary>
        public void LoadDictionaries()
        {
            AlbumsFromArtist = albums.GroupBy(album => album.Artist.Name).ToDictionary(group => group.Key, group => group.AsEnumerable());
            InfoTitlesFromArtist = infoTitles.GroupBy(infoTitle => GetAlbumById(infoTitle.AlbumID).Artist.Name).ToDictionary(group => group.Key, group => group.AsEnumerable());
        }

        /// <summary>
        /// Actualise le morceau en cours CurrentTitle avec le morceau suivant
        /// </summary>
        public void NextTitle()
        {
            if (currentPlaylist == null) return;
            currentPlaying = currentPlaylist.NextTitle();
        }

        /// <summary>
        /// Actualise le morceau en cours CurrentTitle avec le morceau précédent
        /// </summary>
        public void PreviousTitle()
        {
            if (CurrentPlaylist == null) return;
            currentPlaying = currentPlaylist.PreviousTitle();
        }

        /// <summary>
        /// Permet de connaître le titre en cours
        /// </summary>
        /// <returns>Retourne le titre actuel</returns>
        public CustomTitle CurrentTitle()
        {
            if (CurrentPlaylist == null) return null;
            return currentPlaylist.GetCurrentTitle();
        }

        /// <summary>
        /// Permet d'activer/désactiver l'option de boucle
        /// </summary>
        public void Loop()
        {
            if (CurrentPlaylist == null) return;
            currentPlaylist.LoopTitle = !currentPlaylist.LoopTitle;
        }

        /// <summary>
        /// Permet d'activer/désactiver l'option de l'aléatoire
        /// </summary>
        public void Shuffle()
        {
            if (CurrentPlaylist == null) return;
            currentPlaylist.Shuffle = !currentPlaylist.Shuffle;
        }

        /// <summary>
        /// Permet d'ajouter un Album à l'application
        /// </summary>
        /// <param name="album">Album à ajouter</param>
        public void AddAlbum(Album album)
        {
            if (GetAlbumByName(album.Name) != null) return;
            DataManager.AddAlbum(album);
            albums = DataManager.GetAlbums();
        }

        /// <summary>
        /// Permet d'ajouter un CustomTitle à l'application
        /// </summary>
        /// <param name="title">CustomTitle à ajouter</param>
        public void AddCustomTitle(CustomTitle title)
        {
            if (GetInfoTitleByName(title.Name) != null) return;
            DataManager.AddCustomTitle(title);
            customTitles = DataManager.GetCustomTitles();
        }

        /// <summary>
        /// Permet d'ajouter un InfoTitle à l'application
        /// </summary>
        /// <param name="title">InfoTitle à ajouter</param>
        public void AddInfoTitle(InfoTitle title)
        {
            if (GetInfoTitleByName(title.Name) != null) return;
            DataManager.AddInfoTitle(title);
            infoTitles = DataManager.GetInfoTitles();
        }

        /// <summary>
        /// Permet d'ajouter une Playlist à l'application
        /// </summary>
        /// <param name="playlist">Playlist à ajouter</param>
        public void AddPlaylist(Playlist playlist)
        {
            if (GetPlaylistByName(playlist.Name) != null) return;
            DataManager.AddPlaylist(playlist);
            playlists = DataManager.GetPlaylists();
        }

        /// <summary>
        /// Permet d'ajouter un Artist à l'application
        /// </summary>
        /// <param name="artist">Artist à ajouter</param>
        public void AddArtist(Artist artist)
        {
            if (GetArtistByName(artist.Name) != null) return;
            DataManager.AddArtist(artist);
            artists = DataManager.GetArtists();
        }

        /// <summary>
        /// Permet de retirer un Album de l'application
        /// </summary>
        /// <param name="album">Album à retirer</param>
        public void RemoveAlbum(Album album)
        {
            DataManager.RemoveAlbum(album);
            albums = DataManager.GetAlbums();
        }

        /// <summary>
        /// Permet de retirer un CustomTitle de l'application
        /// </summary>
        /// <param name="title">CustomTitle à retirer</param>
        public void RemoveCustomTitle(CustomTitle title)
        {
            DataManager.RemoveCustomTitle(title);
            customTitles = DataManager.GetCustomTitles();
        }

        /// <summary>
        /// Permet de retirer un InfoTitle de l'application
        /// </summary>
        /// <param name="title">InfoTitle à retirer</param>
        public void RemoveInfoTitle(InfoTitle title)
        {
            DataManager.RemoveInfoTitle(title);
            infoTitles = DataManager.GetInfoTitles();
        }

        /// <summary>
        /// Permet de retirer une Playlist de l'application
        /// </summary>
        /// <param name="playlist">Playlist à retirer</param>
        public void RemovePlaylist(Playlist playlist)
        {
            DataManager.RemovePlaylist(playlist);
            playlists = DataManager.GetPlaylists();
        }

        /// <summary>
        /// Permet d'obtenir la liste des objets Playlist de l'application
        /// </summary>
        /// <returns>Reourne la liste des objets Playlist de l'application</returns>
        public ObservableCollection<Playlist> GetPlaylists()
        {
            return DataManager.GetPlaylists();
        }

        /// <summary>
        /// Permet d'obtenir la liste des objets Album de l'application
        /// </summary>
        /// <returns>Reourne la liste des objets Album de l'application</returns>
        public ObservableCollection<Album> GetAlbums()
        {
            return DataManager.GetAlbums();
        }

        /// <summary>
        /// Permet d'obtenir la liste des objets CustomTitle de l'application
        /// </summary>
        /// <returns>Reourne la liste des objets CustomTitle de l'application</returns>
        public ObservableCollection<CustomTitle> GetCustomTitles()
        {
            return DataManager.GetCustomTitles();
        }

        /// <summary>
        /// Permet d'obtenir la liste des objets InfoTitle de l'application
        /// </summary>
        /// <returns>Reourne la liste des objets InfoTitle de l'application</returns>
        public ObservableCollection<InfoTitle> GetInfoTitles()
        {
            return DataManager.GetInfoTitles();
        }

        /// <summary>
        /// Permet d'obtenir la liste des objets Artist de l'application
        /// </summary>
        /// <returns>Reourne la liste des objets Artist de l'application</returns>
        public IEnumerable<Artist> GetArtists()
        {
            return DataManager.GetArtists();
        }

        /// <summary>
        /// Permet de charger l'état sauvegardé précédemment dans l'application
        /// </summary>
        public void LoadSerialization()
        {
            DataManager.LoadSerialization();
        }

        /// <summary>
        /// Permet de sauvegarder l'état de l'application
        /// </summary>
        public void SaveSerialization()
        {
            DataManager.SaveSerialization();
        }

        /// <summary>
        /// Permet d'obtenir une Playlist selon son nom
        /// </summary>
        /// <param name="name">Nom de la Playlist</param>
        /// <returns>Retourne la Playlist correspondant au nom donné en paramètre</returns>
        public Playlist GetPlaylistByName(string name)
        {
            return DataManager.GetPlaylistByName(name);
        }

        /// <summary>
        /// Permet d'obtenir un Artist selon son nom
        /// </summary>
        /// <param name="name">Nom de l'Artist</param>
        /// <returns>Retourne l'Artist correspondant au nom donné en paramètre</returns>
        public Artist GetArtistByName(string name)
        {
            return DataManager.GetArtistByName(name);
        }

        /// <summary>
        /// Permet d'obtenir un CustomTitle selon son chemin d'accès
        /// </summary>
        /// <param name="path">Chemin d'accès du CustomTitle</param>
        /// <returns>Retourne le CustomTitle correspondant au chemin d'accès donné en paramètre</returns>
        public CustomTitle GetCustomTitleByPath(string path)
        {
            return DataManager.GetCustomTitleByPath(path);
        }

        /// <summary>
        /// Permet d'obtenir un InfoTitle selon son nom
        /// </summary>
        /// <param name="name">Nom de l'InfoTitle</param>
        /// <returns>Retourne l'InfoTitle correspondant au nom donné en paramètre</returns>
        public InfoTitle GetInfoTitleByName(string name)
        {
            return DataManager.GetInfoTitleByName(name);
        }

        /// <summary>
        /// Permet d'obtenir un Album selon son nom
        /// </summary>
        /// <param name="name">Nom de l'Album</param>
        /// <returns>Retourne l'Album correspondant au nom donné en paramètre</returns>
        public Album GetAlbumByName(string name)
        {
            return DataManager.GetAlbumByName(name);
        }

        /// <summary>
        /// Permet d'obtenir un Album selon son ID
        /// </summary>
        /// <param name="id">ID de l'Album</param>
        /// <returns>Retourne l'Album correspondant au ID donné en paramètre</returns>
        public Album GetAlbumById(long id)
        {
            return DataManager.GetAlbumById(id);
        }

        /// <summary>
        /// Permet d'enlever un titre supprimé de toutes les playlists
        /// </summary>
        /// <param name="title">Titre à supprimer</param>
        public void RemoveCustomTitleFromPlaylists(CustomTitle title)
        {
            foreach(Playlist p in DataManager.GetPlaylists())
            {
                List<CustomTitle> ToDelete = new();
                foreach (CustomTitle ct in p.Titles.Where(ct => ct.Equals(title)))
                {
                    ToDelete.Add(ct);
                }
                foreach(CustomTitle ct in ToDelete)
                {
                    p.RemoveTitle(ct);
                }
            }
        }

        /// <summary>
        /// Modifie un objet CustomTitle avec les informations données en paramètre
        /// </summary>
        /// <param name="title">CustomTitle à modifier</param>
        /// <param name="name">Nom de l'objet CustomTitle</param>
        /// <param name="url">Chemin d'accès de l'image de l'objet CustomTitle</param>
        /// <param name="info">Informations de l'objet CustomTitle</param>
        /// <param name="path">Chemin d'accès de l'objet CustomTitle</param>
        public static void UpdateCustomTitle(CustomTitle title, string name, string url, string info, string path)
        {
            title.Name = name;
            title.ImageURL = url;
            title.Information = info;
            title.Path = path;
        }

        /// <summary>
        /// Modifie un objet CustomTitle avec les informations données en paramètre
        /// </summary>
        /// <param name="path">Chemin d'accès du CustomTitle à modifier</param>
        /// <param name="name">Nom de l'objet CustomTitle</param>
        /// <param name="newUrl">Chemin d'accès de l'image de l'objet CustomTitle</param>
        /// <param name="info">Informations de l'objet CustomTitle</param>
        /// <param name="newPath">Chemin d'accès de l'objet CustomTitle</param>
        public void UpdateCustomTitleByPath(string path, string name, string newUrl, string info, string newPath)
        {
            CustomTitle title = GetCustomTitleByPath(path);
            if (title != null)
            {
                title.Name = name;
                title.ImageURL = newUrl;
                title.Information = info;
                title.Path = newPath;
            }
        }

        /// <summary>
        /// Modifie un objet InfoTitle avec les informations données en paramètre
        /// </summary>
        /// <param name="title">InfoTitle à modifier</param>
        /// <param name="name">Nom de l'objet InfoTitle</param>
        /// <param name="url">Chemin d'accès de l'image de l'objet InfoTitle</param>
        /// <param name="info">Informations de l'objet InfoTitle</param>
        /// <param name="artist">Artist de l'objet InfoTitle</param>
        /// <param name="description">Description de l'objet InfoTitle</param>
        /// <param name="genre">Genre de l'objet InfoTitle</param>
        public static void UpdateInfoTitle(InfoTitle title, string name, string url, string info, Artist artist, string description, Genre genre)
        {
            title.Name = name;
            title.ImageURL = url;
            title.Information = info;
            title.Description = description;
            title.Genre = genre;
        }

        /// <summary>
        /// Modifie un objet InfoTitle avec les informations données en paramètre
        /// </summary>
        /// <param name="name">Nom de l'objet InfoTitle à modifier</param>
        /// <param name="newUrl">Chemin d'accès de l'image de l'objet InfoTitle</param>
        /// <param name="info">Informations de l'objet InfoTitle</param>
        /// <param name="artist">Artist de l'objet InfoTitle</param>
        /// <param name="description">Description de l'objet InfoTitle</param>
        /// <param name="genre">Genre de l'objet InfoTitle</param>
        public void UpdateInfoTitleByName(string name, string newName, string newUrl, string info, Artist artist, string description, Genre genre)
        {
            InfoTitle title = GetInfoTitleByName(name);
            if (title != null)
            {
                title.Name = newName;
                title.ImageURL = newUrl;
                title.Information = info;
                title.Description = description;
                title.Genre = genre;
            }
        }

        /// <summary>
        /// Modifie un objet Album avec les informations données en paramètre
        /// </summary>
        /// <param name="album">Album à modifier</param>
        /// <param name="name">Nom de l'objet Album</param>
        /// <param name="url">Chemin d'accès de l'image de l'objet Album</param>
        /// <param name="artist">Artist de l'objet Album</param>
        /// <param name="description">Description de l'objet Album</param>
        /// <param name="info">Informations de l'objet Album</param>
        public static void UpdateAlbum(Album album, string name, string url, Artist artist, string description, string info)
        {
            album.Name = name;
            album.ImageURL = url;
            album.Artist = artist;
            album.Description = description;
            album.Information = info;
        }

        /// <summary>
        /// Modifie un objet Album avec les informations données en paramètre
        /// </summary>
        /// <param name="name">Nom de l'objet Album à modifier</param>
        /// <param name="newUrl">Chemin d'accès de l'image de l'objet Album</param>
        /// <param name="artist">Artist de l'objet Album</param>
        /// <param name="description">Description de l'objet Album</param>
        /// <param name="info">Informations de l'objet Album</param>
        public void UpdateAlbumByName(string name, string newName, string newUrl, Artist artist, string description, string info)
        {
            Album album = GetAlbumByName(name);
            if (album != null)
            {
                album.Name = newName;
                album.ImageURL = newUrl;
                album.Artist = artist;
                album.Description = description;
                album.Information = info;
            }
        }

        /// <summary>
        /// Modifie un objet Playlist avec les informations données en paramètre
        /// </summary>
        /// <param name="playlist">Playlist à modifier</param>
        /// <param name="name">Nom de l'objet Playlist</param>
        /// <param name="description">Description de l'objet Playlist</param>
        /// <param name="url">Chemin d'accès de l'image de l'objet Playlist</param>
        public static void UpdatePlaylist(Playlist playlist, string name, string description, string url)
        {
            playlist.Name = name;
            playlist.Description = description;
            playlist.ImageURL = url;
        }

        /// <summary>
        /// Modifie un objet Playlist avec les informations données en paramètre
        /// </summary>
        /// <param name="name">Nom de l'objet Playlist à modifier</param>
        /// <param name="description">Description de l'objet Playlist</param>
        /// <param name="newUrl">Chemin d'accès de l'image de l'objet Playlist</param>
        public void UpdatePlaylistByName(string name, string newName, string description, string newUrl)
        {
            Playlist playlist = GetPlaylistByName(name);
            if (playlist != null)
            {
                playlist.Name = newName;
                playlist.Description = description;
                playlist.ImageURL = newUrl;
            }
        }

        /// <summary>
        /// Modifie un objet Artist avec les informations données en paramètre
        /// </summary>
        /// <param name="artist">Artiste à modifier</param>
        /// <param name="name">Nom de l'objet Artist</param>
        public static void UpdateArtist(Artist artist, string name)
        {
            artist.Name = name;
        }

        /// <summary>
        /// Modifie un objet Artist avec les informations données en paramètre
        /// </summary>
        /// <param name="name">Nom de l'objet Artist à modifier</param>
        /// <param name="newName">Nouveau nom de l'objet Artist</param>
        public void UpdateArtistByName(string name, string newName)
        {
            Artist artist = GetArtistByName(name);
            if (artist != null)
            {
                artist.Name = newName;
            }
        }

        /// <summary>
        /// Permet la notification, et donc l'actualisation des objets connectés à l'événement lorsque la méthode est appelée
        /// </summary>
        /// <param name="propertyName">Nom de la propriété modifiée</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
