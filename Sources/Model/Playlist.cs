﻿using Model.Stub;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;

namespace Model
{
    /// <remarks>
    /// Classe Playlist
    /// </remarks>
    public class Playlist : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public string Name
        {
            get => name;

            set
            {
                if (string.IsNullOrEmpty(value) || value.Length > Manager.MAX_NAME_LENGTH)
                {
                    return;
                }
                name = value;
                OnPropertyChanged();
            }
        }

        private string name = Manager.DEFAULT_NAME;

        public string Description
        {
            get => description;

            set
            {
                if (value != null && value.Length < Manager.MAX_DESCRIPTION_LENGTH)
                {
                    description = value;
                }
                OnPropertyChanged();
            }
        }

        private string description = Manager.DEFAULT_DESC;

        public ObservableCollection<CustomTitle> Titles
        {
            get
            {
                return titles;
            }
            private set
            {
                titles = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<CustomTitle> titles = new ObservableCollection<CustomTitle>();

        public string ImageURL
        {
            get => imageURL;

            set
            {
                if (value == null || !value.Contains('.'))
                {
                    value = "none.png";
                    imageURL = value;
                }
                else if (value.Contains('.'))
                {
                    imageURL = value;
                }
                OnPropertyChanged();
            }
        }

        private string imageURL = Manager.DEFAULT_URL;

        public int Index
        {
            get => index;

            set
            {
                if (value < titles.Count)
                {
                    index = value;
                }
                else
                {
                    index = 0;
                }
            }
        }

        private int index = -1;

        public bool Shuffle { get; set; } = false;

        public bool LoopTitle { get; set; } = false;

        private readonly List<int> played = new List<int>();

        public IEnumerable<int> Played
        {
            get
            {
                return new List<int>(played);
            }
        }

        public bool IsSubMenuVisible
        {
            get => isSubMenuVisible;
            set
            {
                if (isSubMenuVisible != value)
                {
                    isSubMenuVisible = value;
                }
                OnPropertyChanged(nameof(IsSubMenuVisible));
            }
        }

        private bool isSubMenuVisible;

        /// <summary>
        /// Constructeur de la classe Playlist
        /// </summary>
        /// <param name="nom">Nom de la Playlist</param>
        /// <param name="description">Description de la Playlist</param>
        /// <param name="imageURL">Chemin d'accès de l'image de la Playlist</param>
        public Playlist(string nom, string description, string imageURL)
        {
            Name = nom;
            Description = description;
            ImageURL = imageURL;
        }

        /// <summary>
        /// Constructeur par défaut de la Playlist
        /// </summary>
        public Playlist() { }

        /// <summary>
        /// Ajoute un titre à la Playlist
        /// </summary>
        /// <param name="morceau">Morceau à ajouter à la Playlist</param>
        public void AddTitle(CustomTitle morceau)
        {
            titles.Add(morceau);
        }

        /// <summary>
        /// Supprime un titre de la playlist
        /// </summary>
        /// <param name="morceau">Morceau à supprimer</param>
        public void RemoveTitle(CustomTitle morceau)
        {
            titles.Remove(morceau);
        }

        /// <summary>
        /// Permet de connaître le prochain morceau à jouer dans la playlist
        /// </summary>
        /// <returns>Retourne le CustomTitle suivant selon les options indiquées par l'utilisateur</returns>
        /// Vérifie s'il y a des morceaux dans la playlist, puis vérifie si l'option de boucle du morceau est activé.
        /// Si elle est activée, cela renvoie le même morceau. Sinon, si l'option aléatoire N'est PAS activée, cela renvoie
        /// simplement le morceau à la suite dans la liste des titres de la playlist. Sinon, l'option aléatoire est activée, cela
        /// appelle donc une méthode de génération d'un index aléatoire pour avoir un morceau aléatoire dans la liste de la playlist.
        public CustomTitle NextTitle()
        {
            if (titles.Count < 1) return null;
            if (LoopTitle)
            {
                return GetCurrentTitle();
            }

            if (!Shuffle)
            {
                Index++;
                played.Add(Index);
                return GetCurrentTitle();
            }
            else
            {
                Index = RandomGenerator(titles.Count);
                played.Add(Index);
                return GetCurrentTitle();
            }
        }

        /// <summary>
        /// Permet de connaître le titre précédentà jouer dans la playlist
        /// </summary>
        /// <returns>Retourne le CustomTitle précédent selon les options indiquées par l'utilisateur</returns>
        /// Vérifie s'il y a des morceaux dans la playlist, puis vérifie si l'option de boucle du morceau est activé.
        /// Si elle est activée, cela renvoie le même morceau. Sinon, si l'option aléatoire N'est PAS activée, cela renvoie
        /// simplement le morceau précédent celui actuel dans la liste des titres de la playlist. Si le morceau actuel est le
        /// premier, le morceau précédent est le dernier de la playlist. 
        /// Sinon, l'option aléatoire est activée, la liste des morceaux jouée est analysée. Si elle est vide, le morceau
        /// actuel est retourné. Sinon, cela renvoie le dernier élément de la liste des morceaux joués. A noter que cette liste ne
        /// stocke que les index des titres présents dans la playlist.
        public CustomTitle PreviousTitle()
        {
            if (titles.Count < 1) return null;
            if (LoopTitle)
            {
                return GetCurrentTitle();
            }

            if (!Shuffle)
            {
                if (Index != 0)
                {
                    Index--;
                    played.RemoveAt(played.Count - 1);
                    return GetCurrentTitle();
                }
                else
                {
                    return GetCurrentTitle();
                }
            }
            else
            {
                if (!played.Any())
                {
                    return GetCurrentTitle();
                }
                Index = played[played.Count - 1];
                played.RemoveAt(played.Count - 1);
                return GetCurrentTitle();
            }
        }

        /// <summary>
        /// Permet de connaître le morceau actuel de la playlist.
        /// </summary>
        /// <returns>Retourne le morceau actuel de la playlist</returns>
        /// Tout d'abord, des tests de vérification sur l'index sont effectués pour éviter les bugs. Puis, cela renvoie le morceau
        /// à l'index de la playlist dans la liste.
        public CustomTitle GetCurrentTitle()
        {
            if (Index < 0) Index = 0;
            if (Index < titles.Count)
            {
                return titles[Index];
            }
            return null;

        }

        /// <summary>
        /// Fonction qui permet de déterminer si deux objets Playlist sont égaux
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Un booléen indiquant si l'objet est égal</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Playlist)) return false;
            if (obj is Playlist playlist && Name == playlist.Name) return true;
            else return false;
        }

        /// <summary>
        /// Permet de déterminer le hash d'un objet Playlist
        /// </summary>
        /// <returns>Hash de l'attribut Name</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// Permet de convertir un objet Playlist en string
        /// </summary>
        /// <returns>Une nouvelle chaîne caractères contenant le nom de la Playlist</returns>
        public override string ToString()
        {
            return $"Name : {Name}";
        }

        /// <summary>
        /// Permet de générer un nombre entre 0 et n
        /// </summary>
        /// <param name="n">Limite supérieure du nombre à générer</param>
        /// <returns>Retourne un nombre entre 0 et n</returns>
        /// Cette méthode est effectuée via RandomNumberGenerator afin d'obtenir un aléatoire moins prévisible.
        public static int RandomGenerator(int n)
        {
            RandomNumberGenerator rng = RandomNumberGenerator.Create();
            byte[] randomBytes = new byte[4];
            rng.GetBytes(randomBytes);
            uint randomNumber = BitConverter.ToUInt32(randomBytes, 0);
            return (int)(randomNumber % n) + 1;
        }

        /// <summary>
        /// Vérifie si la Playlist contient un certain CustomTitle
        /// </summary>
        /// <param name="customTitle">CustomTitle à vérifier</param>
        /// <returns>Booléen True si le CustomTitle est contenu dans la Playlist, False sinon</returns>
        public bool HasCustomTitle(CustomTitle customTitle)
        {
            if (Titles.Any(ct => ct.Equals(customTitle)))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Permet la notification, et donc l'actualisation des objets connectés à l'événement lorsque la méthode est appelée
        /// </summary>
        /// <param name="propertyName">Nom de la propriété modifiée</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}