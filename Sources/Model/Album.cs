﻿using Model.Stub;
using System.Collections.ObjectModel;

namespace Model
{
    /// <remarks>
    /// Classe Album
    /// </remarks>
    
    public class Album
    {
        private static long nbAlbum = 0;

        private readonly long id;

        public long ID
        {
            get => id;
        }

        public string Name
        {
            get => name;

            set
            {
                if (value != null && value.Length < Manager.MAX_NAME_LENGTH)
                {
                    name = value;
                }
            }
        }

        private string name = Manager.DEFAULT_NAME;

        public string Description
        {
            get => description;

            set
            {
                if (value != null && value.Length < Manager.MAX_DESCRIPTION_LENGTH)
                {
                    description = value;
                }
            }
        }

        private string description = Manager.DEFAULT_DESC;

        public string ImageURL
        {
            get => imageURL;

            set
            {
                if (value == null || !value.Contains('.'))
                {
                    value = Manager.DEFAULT_URL;
                    imageURL = value;
                }
                if (value.Contains(' '))
                {
                    imageURL = value.Replace(' ', '_');
                }
                else if (value.Contains('.'))
                {
                    imageURL = value;
                }
            }
        }

        private string imageURL = Manager.DEFAULT_URL;

        public Artist Artist { get; set; }

        public string Information
        {
            get => information;

            set
            {
                if (value != null && value.Length < Manager.MAX_DESCRIPTION_LENGTH)
                {
                    information = value;
                }
            }
        }

        private string information = Manager.DEFAULT_DESC;

        public ObservableCollection<InfoTitle> InfoTitles { get; set; } = new();

        /// <summary>
        /// Constructeur de la classe Album
        /// </summary>
        /// <param name="name">Nom de l'Album</param>
        /// <param name="imageURL">Chemin d'accès de l'image de l'Album</param>
        /// <param name="artist">Artiste de l'Album</param>
        /// <param name="description">Description de l'Album</param>
        /// <param name="information">Informations complémentaires sur un Album</param>
        public Album(string name, string imageURL, Artist artist, string description, string information)
        {
            id = nbAlbum;
            IncrementNbAlbum();
            Name = name;
            ImageURL = imageURL;
            Artist = artist;
            Description = description;
            Information = information;
        }
        
        /// <summary>
        /// Constructeur par défaut de la classe Album
        /// </summary>
        public Album()
        {
            Artist = new Artist(Manager.DEFAULT_NAME);
            IncrementNbAlbum();
        }

        public static void IncrementNbAlbum()
        {
            nbAlbum++;
        }

        /// <summary>
        /// Permet d'ajouter l'InfoTitle passé en paramètre à la liste
        /// </summary>
        /// <param name="title">Titre à rajouter à l'album</param>
        public void AddTitle(InfoTitle title)
        {
            InfoTitles.Add(title);
        }

        /// <summary>
        /// Permet de supprimer l'InfoTitle passé en paramètre si il est présent dans la liste
        /// </summary>
        /// <param name="title">Titre à supprimer de l'album</param>
        public void RemoveTitle(InfoTitle title)
        {
            InfoTitles.Remove(title);
        }

        /// <summary>
        /// Fonction qui permet de déterminer si deux objets Album sont égaux
        /// </summary>
        /// <param name="obj">Objet comparé à l'album actuel</param>
        /// <returns>Un booléen indiquant si l'objet est égal</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Album)) return false;
            if (obj is Album album && Name == album.Name) return true;
            else return false;
        }

        /// <summary>
        /// Permet de déterminer le hash d'un objet Album
        /// </summary>
        /// <returns>Hash de l'attribut Name</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// Permet de convertir un objet Album en string
        /// </summary>
        /// <returns>Une nouvelle chaîne caractères contenant le nom de l'Album et le nom de l'Artist</returns>
        public override string ToString()
        {
            return $"Name : {Name}, Artist : {Artist}";
        }
    }
}