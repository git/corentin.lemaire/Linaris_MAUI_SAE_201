﻿using System.Collections.ObjectModel;

namespace Model
{
    public interface IDataManager
    {
        // Create

        /// <summary>
        /// Permet d'ajouter un objet Album à l'application
        /// </summary>
        /// <param name="album">Album à ajouter</param>
        void AddAlbum(Album album);

        /// <summary>
        /// Permet d'ajouter des objets Album à l'application
        /// </summary>
        /// <param name="albumsList">Liste d'Album à ajouter</param>
        void AddAlbums(List<Album> albumsList);

        /// <summary>
        /// Permet d'ajouter un objet Artist à l'application
        /// </summary>
        /// <param name="artist">Artist à ajouter</param>
        void AddArtist(Artist artist);

        /// <summary>
        /// Permet d'ajouter des objets Artist à l'application
        /// </summary>
        /// <param name="artistsList">Liste d'Artist à ajouter</param>
        void AddArtists(List<Artist> artistsList);

        /// <summary>
        /// Permet d'ajouter un objet Playlist à l'application
        /// </summary>
        /// <param name="playlist">Playlist à ajouter</param>
        void AddPlaylist(Playlist playlist);

        /// <summary>
        /// Permet d'ajouter des objets Playlist à l'application
        /// </summary>
        /// <param name="playlistsList">Liste de Playlist à ajouter</param>
        void AddPlaylists(List<Playlist> playlistsList);

        /// <summary>
        /// Permet d'ajouter un objet CustomTitle à l'application
        /// </summary>
        /// <param name="title">CustomTitle à ajouter</param>
        void AddCustomTitle(CustomTitle title);

        /// <summary>
        /// Permet d'ajouter des objets CustomTitle à l'application
        /// </summary>
        /// <param name="customTitlesList">Liste de CustomTitle à ajouter</param>
        void AddCustomTitles(List<CustomTitle> customTitlesList);

        /// <summary>
        /// Permet d'ajouter un objet InfoTitle à l'application
        /// </summary>
        /// <param name="title">InfoTitle à ajouter</param>
        void AddInfoTitle(InfoTitle title);

        /// <summary>
        /// Permet d'ajouter des objets InfoTitle à l'application
        /// </summary>
        /// <param name="infoTitlesList">Liste d'InfoTitle à ajouter</param>
        void AddInfoTitles(List<InfoTitle> infoTitlesList);




        // Read

        /// <summary>
        /// Permet d'obtenir une liste d'objets CustomTitle
        /// </summary>
        /// <returns>Retourne une liste d'objets CustomTitle</returns>
        ObservableCollection<CustomTitle> GetCustomTitles();

        /// <summary>
        /// Permet d'obtenir un objet CustomTitle à partir de son chemin d'accès
        /// </summary>
        /// <param name="custom">Chemin d'accès de l'objet CustomTitle</param>
        /// <returns>Retourne l'objet CustomTitle avec un chemin d'accès équivalent à celui donné en paramètre</returns>
        CustomTitle GetCustomTitleByPath(string custom);

        /// <summary>
        /// Permet d'obtenir une liste d'objets InfoTitle
        /// </summary>
        /// <returns>Retourne une liste d'objets InfoTitle</returns>
        ObservableCollection<InfoTitle> GetInfoTitles();

        /// <summary>
        /// Permet d'obtenir un objet InfoTitle à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet InfoTitle</param>
        /// <returns>Retourne l'objet InfoTitle avec un nom équivalent à celui donné en paramètre</returns>
        InfoTitle GetInfoTitleByName(string name);

        /// <summary>
        /// Permet d'obtenir une liste d'objets Album
        /// </summary>
        /// <returns>Retourne une liste d'objets Album</returns>
        ObservableCollection<Album> GetAlbums();

        /// <summary>
        /// Permet d'obtenir un objet Album à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Album</param>
        /// <returns>Retourne l'objet Album avec un nom équivalent à celui donné en paramètre</returns>
        Album GetAlbumByName(string name);

        /// <summary>
        /// Permet d'obtenir un objet Album à partir de son ID
        /// </summary>
        /// <param name="id">ID de l'objet Album</param>
        /// <returns>Retourne l'objet Album avec un ID équivalent à celui donné en paramètre</returns>
        Album GetAlbumById(long id);

        /// <summary>
        /// Permet d'obtenir une liste d'objets Artist
        /// </summary>
        /// <returns>Retourne une liste d'objets Artist</returns>
        List<Artist> GetArtists();

        /// <summary>
        /// Permet d'obtenir un objet Artist à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Artist</param>
        /// <returns>Retourne l'objet Artist avec un nom équivalent à celui donné en paramètre</returns>
        Artist GetArtistByName(string name);

        /// <summary>
        /// Permet d'obtenir une liste d'objets Playlist
        /// </summary>
        /// <returns>Retourne une liste d'objets Playlist</returns>
        ObservableCollection<Playlist> GetPlaylists();

        /// <summary>
        /// Permet d'obtenir un objet Playlist à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Playlist</param>
        /// <returns>Retourne l'objet Playlist avec un nom équivalent à celui donné en paramètre</returns>
        Playlist GetPlaylistByName(string name);





        // Delete

        /// <summary>
        /// Permet de retirer un objet Album de l'application
        /// </summary>
        /// <param name="album">Album à retirer</param>
        void RemoveAlbum(Album album);

        /// <summary>
        /// Permet de retirer des objets Album de l'application
        /// </summary>
        /// <param name="albumsList">Liste des objets Album à retirer de l'application</param>
        void RemoveAlbums(List<Album> albumsList);

        /// <summary>
        /// Permet de retirer un objet Artist de l'application
        /// </summary>
        /// <param name="artist">Artist à retirer</param>
        void RemoveArtist(Artist artist);

        /// <summary>
        /// Permet de retirer des objets Artist de l'application
        /// </summary>
        /// <param name="artistsList">Liste des objets Artist à retirer de l'application</param>
        void RemoveArtists(List<Artist> artistsList);

        /// <summary>
        /// Permet de retirer un objet Playlist de l'application
        /// </summary>
        /// <param name="playlist">Playlist à retirer</param>
        void RemovePlaylist(Playlist playlist);

        /// <summary>
        /// Permet de retirer des objets Playlist de l'application
        /// </summary>
        /// <param name="playlistsList">Liste des objets Playlist à retirer de l'application</param>
        void RemovePlaylists(List<Playlist> playlistsList);

        /// <summary>
        /// Permet de retirer un objet CustomTitle de l'application
        /// </summary>
        /// <param name="title">CustomTitle à retirer</param>
        void RemoveCustomTitle(CustomTitle title);

        /// <summary>
        /// Permet de retirer des objets CustomTitle de l'application
        /// </summary>
        /// <param name="customTitlesList">Liste des objets CustomTitle à retirer de l'application</param>
        void RemoveCustomTitles(List<CustomTitle> customTitlesList);

        /// <summary>
        /// Permet de retirer un objet InfoTitle de l'application
        /// </summary>
        /// <param name="title">InfoTitle à retirer</param>
        void RemoveInfoTitle(InfoTitle title);

        /// <summary>
        /// Permet de retirer des objets InfoTitle de l'application
        /// </summary>
        /// <param name="infoTitlesList">Liste des objets InfoTitle à retirer de l'application</param>
        void RemoveInfoTitles(List<InfoTitle> infoTitlesList);



        // Serialization

        /// <summary>
        /// Permet de charger les données
        /// </summary>
        void LoadSerialization();

        /// <summary>
        /// Permet de sauvegarder l'état de l'application
        /// </summary>
        void SaveSerialization();




        // Exists

        /// <summary>
        /// Permet de savoir si un objet Playlist appartient ou non à l'application
        /// </summary>
        /// <param name="playlist">Playlist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        bool ExistsPlaylist(Playlist playlist);

        /// <summary>
        /// Permet de savoir si un objet Playlist appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Playlist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        bool ExistsPlaylistByName(string name);

        /// <summary>
        /// Permet de savoir si un objet Album appartient ou non à l'application
        /// </summary>
        /// <param name="album">Album à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        bool ExistsAlbum(Album album);

        /// <summary>
        /// Permet de savoir si un objet Album appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Album à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        bool ExistsAlbumByName(string name);

        /// <summary>
        /// Permet de savoir si un objet Artist appartient ou non à l'application
        /// </summary>
        /// <param name="artist">Artist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        bool ExistsArtist(Artist artist);

        /// <summary>
        /// Permet de savoir si un objet Artist appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Artist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        bool ExistsArtistByName(string name);

        /// <summary>
        /// Permet de savoir si un objet CustomTitle appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="title">Nom de l'objet CustomTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        bool ExistsCustomTitle(CustomTitle title);

        /// <summary>
        /// Permet de savoir si un objet CustomTitle appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet CustomTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        bool ExistsCustomTitleByName(string name);

        /// <summary>
        /// Permet de savoir si un objet InfoTitle appartient ou non à l'application
        /// </summary>
        /// <param name="title">InfoTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        bool ExistsInfoTitle(InfoTitle title);

        /// <summary>
        /// Permet de savoir si un objet InfoTitle appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet InfoTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        bool ExistsInfoTitleByName(string name);
    }
}