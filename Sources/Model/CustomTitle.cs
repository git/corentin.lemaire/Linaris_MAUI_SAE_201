﻿using Model.Stub;

namespace Model
{
    /// <remarks>
    /// Classe CustomTitle
    /// </remarks> 
    public class CustomTitle : Title
    {
        public string Path
        {
            get => path;

            set
            {
                if (value != null && value.Length < Manager.MAX_DESCRIPTION_LENGTH)
                {
                    path = value;
                }
                OnPropertyChanged();
            }
        }

        private string path = Manager.DEFAULT_URL;

        public bool IsSubMenuVisible
        {
            get => isSubMenuVisible;
            set
            {
                if (isSubMenuVisible != value)
                {
                    isSubMenuVisible = value;
                }
                OnPropertyChanged();
            }
        }

        private bool isSubMenuVisible = false;

        public bool IsPlaylistMenuVisible
        {
            get => isPlaylistMenuVisible;
            set
            {
                if (isPlaylistMenuVisible != value)
                {
                    isPlaylistMenuVisible = value;
                }
                OnPropertyChanged();
            }
        }

        private bool isPlaylistMenuVisible = false;

        public bool IsNewPlaylistMenuVisible
        {
            get => isNewPlaylistMenuVisible;
            set
            {
                if (isNewPlaylistMenuVisible != value)
                {
                    isNewPlaylistMenuVisible = value;
                }
                OnPropertyChanged();
            }
        }

        private bool isNewPlaylistMenuVisible = false;

        /// <summary>
        /// Constructeur de la classe Album
        /// </summary>
        /// <param name="name">Nom du CustomTitle</param>
        /// <param name="imageURL">Chemin d'accès de l'image du CustomTitle</param>
        /// <param name="information">Informations sur un CustomTitle</param>
        /// <param name="path">Chemin d'accès du CustomTitle</param>
        public CustomTitle(string name, string imageURL, string information, string path) : base(name, imageURL, information)
        {
            Path = path;
        }

        /// <summary>
        /// Constructeur par défaut de la classe CustomTitle
        /// </summary>
        public CustomTitle() : base(Manager.DEFAULT_NAME, Manager.DEFAULT_URL, Manager.DEFAULT_DESC) { }

        /// <summary>
        /// Fonction qui permet de déterminer si deux objets CustomTitle sont égaux selon leurs chemin d'accès
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Un booléen indiquant si l'objet est égal</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(CustomTitle)) return false;
            if (obj is CustomTitle custom && Path == custom.Path) return true;
            else return false;
        }

        /// <summary>
        /// Permet de déterminer le hash d'un objet CustomTitle
        /// </summary>
        /// <returns>Hash de l'attribut Path</returns>
        public override int GetHashCode()
        {
            return Path.GetHashCode();
        }

        /// <summary>
        /// Permet de convertir un objet CustomTitle en string
        /// </summary>
        /// <returns>Une nouvelle chaîne caractères contenant le nom du CustomTitle et le chemin d'accès</returns>
        public override string ToString()
        {
            return $"Name : {Name}, Path : {Path}";
        }
    }
}