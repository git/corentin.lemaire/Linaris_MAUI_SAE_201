﻿using System.Xml.Serialization;
using Model.Stub;

namespace Model
{
    /// <remarks>
    /// Classe InfoTitle
    /// </remarks>
    public class InfoTitle : Title
    {

        public string Description
        {
            get => description;

            set
            {
                if (value != null && value.Length < Manager.MAX_DESCRIPTION_LENGTH)
                {
                    description = value;
                }
            }
        }

        private string description = Manager.DEFAULT_DESC;

        private List<Artist> feat = new();

        public IEnumerable<Artist> Feat
        {
            get
            {
                return new List<Artist>(feat);
            }
        }

        public Genre Genre { get; set; }

        public long AlbumID { get; set; }

        /// <summary>
        /// Constructeur de la classe InfoTitle
        /// </summary>
        /// <param name="name">Nom de l'InfoTitle</param>
        /// <param name="imageURL">Chemin d'accès de l'image de l'InfoTitle</param>
        /// <param name="information">Informations complémentaires sur l'InfoTitle</param>
        /// <param name="description">Description de l'InfoTitle</param>
        /// <param name="genre">Genre de l'InfoTitle</param>
        /// <param name="albumID">ID de l'album auquel appartient l'InfoTitle</param>
        public InfoTitle(string name, string imageURL, string description, string information, Genre genre, long albumID) : base(name, imageURL, information)
        {
            Description = description;
            Genre = genre;
            AlbumID = albumID;
        }

        /// <summary>
        /// Constructeur par défaut de la classe InfoTitle
        /// </summary>
        public InfoTitle() : base(Manager.DEFAULT_NAME, Manager.DEFAULT_URL, Manager.DEFAULT_DESC)
        {

        }

        /// <summary>
        /// Ajoute un artiste en tant que Feat de l'InfoTitle
        /// </summary>
        /// <param name="artist"></param>
        public void AddFeat(Artist artist)
        {
            feat.Add(artist);
        }

        /// <summary>
        /// Enlève un artiste des feats de l'InfoTitle
        /// </summary>
        /// <param name="artiste"></param>
        public void RemoveFeat(Artist artiste)
        {
            foreach (var item in Feat)
            {
                feat = Feat.Where(item => item != artiste).ToList();
            }
        }

        /// <summary>
        /// Fonction qui permet de déterminer si deux objets InfoTitle sont égaux
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Un booléen indiquant si l'objet est égal</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(InfoTitle)) return false;
            if (obj is InfoTitle infoTitle && Name == infoTitle.Name) return true;
            else return false;
        }

        /// <summary>
        /// Permet de déterminer le hash d'un objet InfoTitle
        /// </summary>
        /// <returns>Hash de l'attribut Name</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// Permet de convertir un objet InfoTitle en string
        /// </summary>
        /// <returns>Une nouvelle chaîne caractères contenant le nom de l'InfoTitle et le chemin d'accès à l'image</returns>
        public override string ToString()
        {
            return $"Name : {Name}, ImageUrl : {ImageURL}";
        }
    }
}