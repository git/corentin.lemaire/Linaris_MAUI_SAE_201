﻿using Model.Stub;
using System.Xml.Serialization;

namespace Model
{
    /// <remarks>
    /// Classe Artist
    /// </remarks>
    public class Artist
    {

        public string Name
        {
            get => name;

            set
            {
                if (value != null && value.Length < Manager.MAX_NAME_LENGTH)
                {
                    name = value;
                }
            }
        }

        private string name = Manager.DEFAULT_NAME;

        /// <summary>
        /// Constructeur de la classe Artist
        /// </summary>
        /// <param name="name">Nom de l'artiste</param>
        public Artist(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Constructeur par défaut de la classe Artist
        /// </summary>
        public Artist()
        {

        }

        /// <summary>
        /// Fonction qui permet de déterminer si deux objets Artist sont égaux
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Un booléen indiquant si l'objet est égal</returns>
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != typeof(Artist)) return false;
            if (obj is Artist artist && Name == artist.Name) return true;
            else return false;
        }

        /// <summary>
        /// Permet de déterminer le hash d'un objet Artist
        /// </summary>
        /// <returns>Hash de l'attribut Name</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// Permet de convertir un objet Artist en string
        /// </summary>
        /// <returns>Une nouvelle chaîne caractères contenant le nom de l'Artist</returns>
        public override string ToString()
        {
            return $"{Name}";
        }
    }
}