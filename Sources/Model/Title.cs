﻿using Model.Stub;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;

namespace Model
{
    /// <remarks>
    /// Classe Title
    /// </remarks>
    public class Title : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public string Name
        {
            get => name;

            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > Manager.MAX_NAME_LENGTH)
                {
                    value = "IncorrectName";
                }
                name = value;
                OnPropertyChanged();
            }
        }

        private string name = Manager.DEFAULT_NAME;

        public string ImageURL
        {
            get => imageURL;

            set
            {
                if (value == null || !value.Contains('.'))
                {
                    value = "none.png";
                    imageURL = value;
                }
                else if (value.Contains('.'))
                {
                    imageURL = value;
                }
                OnPropertyChanged();
            }
        }

        private string imageURL = Manager.DEFAULT_URL;

        public string Information
        {
            get => information;

            set
            {
                if (value != null && value.Length < Manager.MAX_DESCRIPTION_LENGTH)
                {
                    information = value;
                }
                OnPropertyChanged();
            }
        }

        private string information = Manager.DEFAULT_DESC;

        /// <summary>
        /// Constructeur de la classe Title
        /// </summary>
        /// <param name="nom">Nom du Title</param>
        /// <param name="file_Name">Chemin d'accès de l'image du Title</param>
        /// <param name="informations">Informations sur le Title</param>
        public Title(string nom, string file_Name, string informations)
        {
            Name = nom;
            ImageURL = file_Name;
            Information = informations;
        }

        /// <summary>
        /// Fonction qui permet de déterminer si deux objets Title sont égaux
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Un booléen indiquant si l'objet est égal</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Title)) return false;
            if (obj is Title title && Name == title.Name) return true;
            else return false;
        }

        /// <summary>
        /// Permet de déterminer le hash d'un objet Title
        /// </summary>
        /// <returns>Hash de l'attribut Name</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// Permet de convertir un objet Title en string
        /// </summary>
        /// <returns>Une nouvelle chaîne caractères contenant le nom du Title</returns>
        public override string ToString()
        {
            return $"Name : {Name}";
        }

        /// <summary>
        /// Permet la notification, et donc l'actualisation des objets connectés à l'événement lorsque la méthode est appelée
        /// </summary>
        /// <param name="propertyName">Nom de la propriété modifiée</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}