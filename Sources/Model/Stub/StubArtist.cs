﻿namespace Model.Stub { 
    public class StubArtist
    {
        public List<Artist> Artists
        {
            get => artists;
        }

        private readonly List<Artist> artists;

        /// <summary>
        /// Constructeur de la classe StubArtist
        /// </summary>
        public StubArtist()
        {
            artists = new List<Artist>()
            {
                new Artist("Imagine Dragons"),
                new Artist("Nepal"),
                new Artist("Hugo TSR"),
                new Artist("Booba"),
                new Artist("Oxmo Puccino"),
                new Artist("IAM"),
                new Artist("PNL")
            };
        }

        /// <summary>
        /// Permet d'obtenir la liste des artistes
        /// </summary>
        /// <returns>Retourne la liste des artistes</returns>
        public List<Artist> GetArtists()
        {
            return artists;
        }

        /// <summary>
        /// Permet d'obtenir un artiste selon son nom
        /// </summary>
        /// <param name="name">Nom de l'artiste recherché</param>
        /// <returns>Retourne l'artiste correspondant au nom donné en paramètre</returns>
        public Artist GetArtistByName(string name)
        {
            foreach (var artist in artists)
            {
                if (artist.Name == name)
                {
                    return artist;
                }
            }
            return null;
        }

        /// <summary>
        /// Permet d'ajouter un artiste à la liste des artistes 
        /// </summary>
        /// <param name="artist">Artiste à ajouter</param>
        public void AddArtist(Artist artist)
        {
            artists.Add(artist);
        }

        /// <summary>
        /// Permet de retirer un artiste de la liste des artistes 
        /// </summary>
        /// <param name="artist">Artiste à retirer</param>
        public void RemoveArtist(Artist artist)
        {
            artists.Remove(artist);
        }
    }
}