﻿using System.Collections.ObjectModel;

namespace Model.Stub
{
    public class StubPlaylist
    {

        public ObservableCollection<Playlist> Playlists
        {
            get => playlists;
        }

        private readonly ObservableCollection<Playlist> playlists;

        /// <summary>
        /// Constructeur de la classe StubPlaylist
        /// </summary>
        public StubPlaylist()
        {
            Playlist Playlist1 = new Playlist("Playlist1", "desc1", "url1.png");
            Playlist Playlist2 = new Playlist("Playlist2", "desc2", "url2.png");
            Playlist Playlist3 = new Playlist("Playlist3", "desc3", "url3.png");
            Playlist Playlist4 = new Playlist("Playlist4", "desc4", "url4.png");
            Playlist Playlist5 = new Playlist("Playlist5", "desc5", "url5.png");
            Playlist Playlist6 = new Playlist("Playlist6", "desc6", "url6.png");
            Playlist Playlist7 = new Playlist("Playlist7", "desc7", "url7.png");
            Playlist Playlist8 = new Playlist("Playlist8", "desc8", "url8.png");
            Playlist Playlist9 = new Playlist("Playlist9", "desc9", "url9.png");
            playlists = new ObservableCollection<Playlist>()
            {
                Playlist1, Playlist2, Playlist3, Playlist4, Playlist5, Playlist6, Playlist7, Playlist8, Playlist9
            };
        }

        /// <summary>
        /// Permet d'obtenir la liste des Playlist
        /// </summary>
        /// <returns>Retourne la liste des Playlist</returns>
        public ObservableCollection<Playlist> GetPlaylists()
        {
            return playlists;
        }

        /// <summary>
        /// Permet d'obtenir une Playlist selon son nom
        /// </summary>
        /// <param name="name">Nom de la Playlist recherchée</param>
        /// <returns>Retourne la Playlist correspondant au nom donné en paramètre</returns>
        public Playlist GetPlaylistByName(string name)
        {
            foreach (var playlist in playlists)
            {
                if (playlist.Name == name)
                {
                    return playlist;
                }
            }
            return null;
        }

        /// <summary>
        /// Permet d'ajouter une Playlist à la liste des Playlist
        /// </summary>
        /// <param name="playlist"></param>
        public void AddPlaylist(Playlist playlist)
        {
            playlists.Add(playlist);
        }

        /// <summary>
        /// Permet de retirer une Playlist de la liste des Playlist
        /// </summary>
        /// <param name="playlist"></param>
        public void RemovePlaylist(Playlist playlist)
        {
            playlists.Remove(playlist);
        }
    }
}