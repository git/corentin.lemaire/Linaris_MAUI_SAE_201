﻿using System.Collections.ObjectModel;

namespace Model.Stub
{
    public class StubCustomTitle
    {

        public ObservableCollection<CustomTitle> CustomTitles
        {
            get => customTitles;
        }

        private readonly ObservableCollection<CustomTitle> customTitles;

        /// <summary>
        /// Constructeur de la classe StubCustomTitle
        /// </summary>
        public StubCustomTitle()
        {
            CustomTitle Custom1 = new CustomTitle("MaMusique", "mp3.png", "info1", "chemin1");
            CustomTitle Custom2 = new CustomTitle("MusiqueGeniale", "wav.png", "info2", "chemin2");
            CustomTitle Custom3 = new CustomTitle("custom3", "midi.png", "info3", "chemin3");
            CustomTitle Custom4 = new CustomTitle("custom4", "ogg.png", "info4", "chemin4");
            CustomTitle Custom5 = new CustomTitle("custom5", "mp3.png", "info5", "chemin5");
            CustomTitle Custom6 = new CustomTitle("custom6", "mp3.png", "info6", "chemin6");
            CustomTitle Custom7 = new CustomTitle("custom7", "wav.png", "info7", "chemin7");
            CustomTitle Custom8 = new CustomTitle("custom8", "ogg.png", "info8", "chemin8");
            CustomTitle Custom9 = new CustomTitle("custom9", "mp3.png", "info9", "chemin9");
            CustomTitle Custom10 = new CustomTitle("custom10", "wav.png", "info10", "chemin10");
            CustomTitle Custom11 = new CustomTitle("custom11", "mp3.png", "info11", "chemin11");

            customTitles = new ObservableCollection<CustomTitle>()
            {
                Custom1, Custom2, Custom3, Custom4, Custom5, Custom6, Custom7, Custom8, Custom9, Custom10, Custom11
            };
        }

        /// <summary>
        /// Permet d'obtenir la liste des CustomTitle
        /// </summary>
        /// <returns>Retourne la liste des CustomTitle</returns>
        public ObservableCollection<CustomTitle> GetCustomTitles()
        {
            return customTitles;
        }

        /// <summary>
        /// Permet d'obtenir une liste de CustomTitle selon les noms donnés en paramètre
        /// </summary>
        /// <param name="names">Liste des noms des CustomTitle recherchés</param>
        /// <returns>Retourne la liste des CustomTitle correspondant aux noms donnés en paramètre</returns>
        public List<CustomTitle> GetCustomTitlesByNames(List<string> names)
        {
            List<CustomTitle> Customs = new List<CustomTitle>();
            foreach (var name in names)
            {
                foreach (var title in customTitles)
                {
                    if (name == title.Name)
                    {
                        Customs.Add(title);
                    }
                }
            }
            return Customs;
        }

        /// <summary>
        /// Permet d'ajouter un CustomTitle à la liste des CustomTitle
        /// </summary>
        /// <param name="customTitle">CustomTitle à ajouter</param>
        public void AddCustomTitle(CustomTitle customTitle)
        {
            customTitles.Add(customTitle);
        }

        /// <summary>
        /// Permet de retirer un CustomTitle de la liste des CustomTitle
        /// </summary>
        /// <param name="customTitle">CustomTitle à retirer</param>
        public void RemoveCustomTitle(CustomTitle customTitle)
        {
            customTitles.Remove(customTitle);
        }
    }
}