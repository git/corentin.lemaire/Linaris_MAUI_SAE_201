﻿using System.Collections.ObjectModel;

namespace Model.Stub
{
    public class StubManager : IDataManager
    {
        // Stubs

        public StubAlbum StubAlbum
        {
            get
            {
                return stubAlbum;
            }
        }

        private readonly StubAlbum stubAlbum;

        public StubArtist StubArtist
        {
            get
            {
                return stubArtist;
            }
        }

        private readonly StubArtist stubArtist;

        public StubCustomTitle StubCustomTitle
        {
            get
            {
                return stubCustomTitle;
            }
        }

        private readonly StubCustomTitle stubCustomTitle;

        public StubInfoTitle StubInfoTitle
        {
            get
            {
                return stubInfoTitle;
            }
        }

        private readonly StubInfoTitle stubInfoTitle;

        public StubPlaylist StubPlaylist
        {
            get
            {
                return stubPlaylist;
            }
        }

        private readonly StubPlaylist stubPlaylist;

        /// <summary>
        /// Constructeut de la classe StubManager
        /// </summary>
        public StubManager()
        {
            stubInfoTitle = new StubInfoTitle();
            stubAlbum = StubInfoTitle.StubAlbum;
            stubArtist = StubAlbum.StubArtist;
            stubCustomTitle = new StubCustomTitle();
            stubPlaylist = new StubPlaylist();
            LoadSerialization();
        }

        /// <summary>
        /// Permet d'obtenir la liste des albums 
        /// </summary>
        /// <returns>Retourne la liste des albums</returns>
        public ObservableCollection<Album> GetAlbums()
        {
            return StubAlbum.GetAlbums();
        }

        /// <summary>
        /// Permet d'obtenir la liste des artistes
        /// </summary>
        /// <returns>Retourne la liste des artistes</returns>
        public List<Artist> GetArtists()
        {
            return StubArtist.GetArtists();
        }

        /// <summary>
        /// Permet d'obtenir la liste des playlists
        /// </summary>
        /// <returns>Retourne la liste des playlists</returns>
        public ObservableCollection<Playlist> GetPlaylists()
        {
            return StubPlaylist.GetPlaylists();
        }

        /// <summary>
        /// Permet d'obtenir la liste des CustomTitles
        /// </summary>
        /// <returns>Retourne la liste des CustomTitles</returns>
        public ObservableCollection<CustomTitle> GetCustomTitles()
        {
            return StubCustomTitle.GetCustomTitles();
        }

        /// <summary>
        /// Permet d'obtenir la liste des InfoTitles
        /// </summary>
        /// <returns>Retourne la liste des InfoTitles</returns>
        public ObservableCollection<InfoTitle> GetInfoTitles()
        {
            return StubInfoTitle.GetInfoTitles();
        }

        /// <summary>
        /// Permet d'ajouter un objet Album à l'application
        /// </summary>
        /// <param name="album">Album à ajouter</param>
        public void AddAlbum(Album album)
        {
            StubAlbum.AddAlbum(album);
        }

        /// <summary>
        /// Permet d'ajouter un objet CustomTitle à l'application
        /// </summary>
        /// <param name="title">CustomTitle à ajouter</param>
        public void AddCustomTitle(CustomTitle title)
        {
            StubCustomTitle.AddCustomTitle(title);
        }

        /// <summary>
        /// Permet d'ajouter un objet InfoTitle à l'application
        /// </summary>
        /// <param name="title">InfoTitle à ajouter</param>
        public void AddInfoTitle(InfoTitle title)
        {
            StubInfoTitle.AddInfoTitle(title);
        }

        /// <summary>
        /// Permet d'ajouter un artiste à la liste des feat d'un InfoTitle
        /// </summary>
        /// <param name="infoTitle">InfoTitle dans lequel ajouter le feat</param>
        /// <param name="artist">Artist à ajouter dans la liste des feats de l'InfoTitle</param>
        public static void AddFeat(InfoTitle infoTitle, Artist artist)
        {
            StubInfoTitle.AddFeat(infoTitle, artist);
        }

        /// <summary>
        /// Permet d'ajouter un objet Playlist à l'application
        /// </summary>
        /// <param name="playlist">Playlist à ajouter</param>
        public void AddPlaylist(Playlist playlist)
        {
            StubPlaylist.AddPlaylist(playlist);
        }

        /// <summary>
        /// Permet d'ajouter un objet Artist à l'application
        /// </summary>
        /// <param name="artist">Artist à ajouter</param>
        public void AddArtist(Artist artist)
        {
            StubArtist.AddArtist(artist);
        }


        /// <summary>
        /// Permet de retirer un objet Album de l'application
        /// </summary>
        /// <param name="album">Album à retirer</param>
        public void RemoveAlbum(Album album)
        {
            StubAlbum.RemoveAlbum(album);
        }

        /// <summary>
        /// Permet de retirer un objet CustomTitle de l'application
        /// </summary>
        /// <param name="title">CustomTitle à retirer</param>
        public void RemoveCustomTitle(CustomTitle title)
        {
            StubCustomTitle.RemoveCustomTitle(title);
        }

        /// <summary>
        /// Permet de retirer un objet InfoTitle de l'application
        /// </summary>
        /// <param name="title">InfoTitle à retirer</param>
        public void RemoveInfoTitle(InfoTitle title)
        {
            StubInfoTitle.RemoveInfoTitle(title);
        }

        /// <summary>
        /// Permet de retirer un objet Playlist de l'application
        /// </summary>
        /// <param name="playlist">Playlist à retirer</param>
        public void RemovePlaylist(Playlist playlist)
        {
            StubPlaylist.RemovePlaylist(playlist);
        }

        /// <summary>
        /// Permet de retirer un objet Artist de l'application
        /// </summary>
        /// <param name="artist">Artist à retirer</param>
        public void RemoveArtist(Artist artist)
        {
            StubArtist.RemoveArtist(artist);
        }

        /// <summary>
        /// Permet de charger les données
        /// </summary>
        public void LoadSerialization()
        {
            foreach (InfoTitle infoTitle in StubInfoTitle.InfoTitles)
            {
                GetAlbumById(infoTitle.AlbumID)?.AddTitle(infoTitle);
            }
        }

        /// <summary>
        /// Permet de sauvegarder l'état de l'application
        /// </summary>
        public void SaveSerialization()
        {
            // Doesn't do anything because it's Stubs
        }

        /// <summary>
        /// Permet d'obtenir un objet CustomTitle à partir de son chemin d'accès
        /// </summary>
        /// <param name="custom">Chemin d'accès de l'objet CustomTitle</param>
        /// <returns>Retourne l'objet CustomTitle avec un chemin d'accès équivalent à celui donné en paramètre</returns>
        public CustomTitle GetCustomTitleByPath(string custom)
        {
            return StubCustomTitle.GetCustomTitles().FirstOrDefault(customTitle => customTitle.Path == custom);
        }

        /// <summary>
        /// Permet d'obtenir un objet InfoTitle à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet InfoTitle</param>
        /// <returns>Retourne l'objet InfoTitle avec un nom équivalent à celui donné en paramètre</returns>
        public InfoTitle GetInfoTitleByName(string name)
        {
            return StubInfoTitle.GetInfoTitles().FirstOrDefault(title => title.Name == name);
        }

        /// <summary>
        /// Permet d'obtenir un objet Album à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Album</param>
        /// <returns>Retourne l'objet Album avec un nom équivalent à celui donné en paramètre</returns>
        public Album GetAlbumByName(string name)
        {
            return StubAlbum.GetAlbums().FirstOrDefault(album => album.Name == name);
        }

        /// <summary>
        /// Permet d'obtenir un objet Artist à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Artist</param>
        /// <returns>Retourne l'objet Artist avec un nom équivalent à celui donné en paramètre</returns>
        public Artist GetArtistByName(string name)
        {
            return StubArtist.GetArtists().FirstOrDefault(artist => artist.Name == name);
        }

        /// <summary>
        /// Permet d'ajouter des objets Album à l'application
        /// </summary>
        /// <param name="albumsList">Liste d'Album à ajouter</param>
        public void AddAlbums(List<Album> albumsList)
        {
            foreach (Album a in albumsList)
            {
                StubAlbum.AddAlbum(a);
            }
        }

        /// <summary>
        /// Permet d'ajouter des objets Artist à l'application
        /// </summary>
        /// <param name="artistsList">Liste d'Artist à ajouter</param>
        public void AddArtists(List<Artist> artistsList)
        {
            foreach (Artist a in artistsList)
            {
                StubArtist.AddArtist(a);
            }
        }

        /// <summary>
        /// Permet d'ajouter des objets Playlist à l'application
        /// </summary>
        /// <param name="playlistsList">Liste de Playlist à ajouter</param>
        public void AddPlaylists(List<Playlist> playlistsList)
        {
            foreach (Playlist p in playlistsList)
            {
                StubPlaylist.AddPlaylist(p);
            }
        }

        /// <summary>
        /// Permet d'ajouter des objets CustomTitle à l'application
        /// </summary>
        /// <param name="customTitlesList">Liste de CustomTitle à ajouter</param>
        public void AddCustomTitles(List<CustomTitle> customTitlesList)
        {
            foreach (CustomTitle ct in customTitlesList)
            {
                StubCustomTitle.AddCustomTitle(ct);
            }
        }

        /// <summary>
        /// Permet d'ajouter des objets InfoTitle à l'application
        /// </summary>
        /// <param name="infoTitlesList">Liste d'InfoTitle à ajouter</param>
        public void AddInfoTitles(List<InfoTitle> infoTitlesList)
        {
            foreach (InfoTitle it in infoTitlesList)
            {
                StubInfoTitle.AddInfoTitle(it);
            }
        }

        /// <summary>
        /// Permet d'obtenir un objet Playlist à partir de son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Playlist</param>
        /// <returns>Retourne l'objet Playlist avec un nom équivalent à celui donné en paramètre</returns>
        public Playlist GetPlaylistByName(string name)
        {
            return StubPlaylist.Playlists.FirstOrDefault(p => p.Name == name);
        }

        /// <summary>
        /// Permet de retirer des objets Album de l'application
        /// </summary>
        /// <param name="albumsList">Liste des objets Album à retirer de l'application</param>
        public void RemoveAlbums(List<Album> albumsList)
        {
            foreach (Album album in albumsList)
            {
                StubAlbum.RemoveAlbum(album);
            }
        }

        /// <summary>
        /// Permet de retirer des objets Artist de l'application
        /// </summary>
        /// <param name="artistsList">Liste des objets Artist à retirer de l'application</param>
        public void RemoveArtists(List<Artist> artistsList)
        {
            foreach (Artist artist in artistsList)
            {
                StubArtist.RemoveArtist(artist);
            }
        }

        /// <summary>
        /// Permet de retirer des objets Playlist de l'application
        /// </summary>
        /// <param name="playlistsList">Liste des objets Playlist à retirer de l'application</param>
        public void RemovePlaylists(List<Playlist> playlistsList)
        {
            foreach (Playlist playlist in playlistsList)
            {
                StubPlaylist.RemovePlaylist(playlist);
            }
        }

        /// <summary>
        /// Permet de retirer des objets CustomTitle de l'application
        /// </summary>
        /// <param name="customTitlesList">Liste des objets CustomTitle à retirer de l'application</param>
        public void RemoveCustomTitles(List<CustomTitle> customTitlesList)
        {
            foreach (CustomTitle customTitle in customTitlesList)
            {
                StubCustomTitle.RemoveCustomTitle(customTitle);
            }
        }

        /// <summary>
        /// Permet de retirer des objets InfoTitle de l'application
        /// </summary>
        /// <param name="infoTitlesList">Liste des objets InfoTitle à retirer de l'application</param>
        public void RemoveInfoTitles(List<InfoTitle> infoTitlesList)
        {
            foreach (InfoTitle infoTitle in infoTitlesList)
            {
                StubInfoTitle.RemoveInfoTitle(infoTitle);
            }
        }

        /// <summary>
        /// Permet de savoir si un objet Playlist appartient ou non à l'application
        /// </summary>
        /// <param name="playlist">Playlist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsPlaylist(Playlist playlist)
        {
            return StubPlaylist.Playlists.Any(p => p.Equals(playlist));
        }

        /// <summary>
        /// Permet de savoir si un objet Playlist appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Playlist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsPlaylistByName(string name)
        {
            return StubPlaylist.Playlists.Any(p => p.Name.Equals(name));
        }

        /// <summary>
        /// Permet de savoir si un objet Album appartient ou non à l'application
        /// </summary>
        /// <param name="album">Album à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsAlbum(Album album)
        {
            return StubAlbum.Albums.Any(a => a.Equals(album));
        }

        /// <summary>
        /// Permet de savoir si un objet Album appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Album à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsAlbumByName(string name)
        {
            return StubAlbum.Albums.Any(a => a.Name == name);
        }

        /// <summary>
        /// Permet de savoir si un objet Artist appartient ou non à l'application
        /// </summary>
        /// <param name="artist">Artist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsArtist(Artist artist)
        {
            return StubArtist.Artists.Any(a => a.Equals(artist));
        }

        /// <summary>
        /// Permet de savoir si un objet Artist appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet Artist à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsArtistByName(string name)
        {
            return StubArtist.Artists.Any(a => a.Name == name);
        }

        /// <summary>
        /// Permet de savoir si un objet CustomTitle appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="title">Nom de l'objet CustomTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsCustomTitle(CustomTitle title)
        {
            return StubCustomTitle.CustomTitles.Any(ct => ct.Equals(title));
        }

        /// <summary>
        /// Permet de savoir si un objet CustomTitle appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet CustomTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsCustomTitleByName(string name)
        {
            return StubCustomTitle.CustomTitles.Any(ct => ct.Name == name);
        }

        /// <summary>
        /// Permet de savoir si un objet InfoTitle appartient ou non à l'application
        /// </summary>
        /// <param name="title">InfoTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsInfoTitle(InfoTitle title)
        {
            return StubInfoTitle.InfoTitles.Any(it => it.Equals(title));
        }

        /// <summary>
        /// Permet de savoir si un objet InfoTitle appartient ou non à l'application selon son nom
        /// </summary>
        /// <param name="name">Nom de l'objet InfoTitle à vérifier</param>
        /// <returns>Booléen True s'il est contenu dans l'application, False sinon</returns>
        public bool ExistsInfoTitleByName(string name)
        {
            return StubInfoTitle.InfoTitles.Any(it => it.Name == name);
        }

        /// <summary>
        /// Permet d'obtenir un objet Album à partir de son ID
        /// </summary>
        /// <param name="id">ID de l'objet Album</param>
        /// <returns>Retourne l'objet Album avec un ID équivalent à celui donné en paramètre</returns>
        public Album GetAlbumById(long id)
        {
            return StubAlbum.Albums.FirstOrDefault(album => album.ID == id);
        }
    }
}