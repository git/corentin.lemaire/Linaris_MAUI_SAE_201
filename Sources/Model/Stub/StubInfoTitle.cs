﻿using System.Collections.ObjectModel;

namespace Model.Stub
{
    public class StubInfoTitle
    {
        public StubAlbum StubAlbum
        {
            get
            {
                return stubAlbum;
            }
        }

        private readonly StubAlbum stubAlbum;

        public ObservableCollection<InfoTitle> InfoTitles
        {
            get => infoTitles;
        }

        private readonly ObservableCollection<InfoTitle> infoTitles;

        /// <summary>
        /// Constructeur de la classe StubInfoTitle
        /// </summary>
        public StubInfoTitle()
        {
            stubAlbum = new StubAlbum();
            Artist ImagineDragons = StubAlbum.StubArtist.GetArtistByName("Imagine Dragons") ?? new Artist("Imagine Dragons");
            Artist PNL = StubAlbum.StubArtist.GetArtistByName("PNL") ?? new Artist("PNL");
            Artist Nepal = StubAlbum.StubArtist.GetArtistByName("Nepal") ?? new Artist("Nepal");
            Artist Booba = StubAlbum.StubArtist.GetArtistByName("Booba") ?? new Artist("Booba");
            Artist IAM = StubAlbum.StubArtist.GetArtistByName("IAM") ?? new Artist("IAM");
            Artist Hugo = StubAlbum.StubArtist.GetArtistByName("Hugo TSR") ?? new Artist("Hugo TSR");
            Artist Oxmo = StubAlbum.StubArtist.GetArtistByName("Oxmo Puccino") ?? new Artist("Oxmo Puccino");
            Album MercuryAct2 = stubAlbum.GetAlbumByName("Mercury Act 2") ?? new Album("Mercury Act 2", "album14.png", ImagineDragons, "desc", "infos");
            Album MercuryAct1 = stubAlbum.GetAlbumByName("Mercury Act 1") ?? new Album("Mercury Act 1", "album13.png", ImagineDragons, "desc", "infos");
            Album Origins = stubAlbum.GetAlbumByName("Origins") ?? new Album("Origins", "album12.png", ImagineDragons, "desc", "infos");
            Album Evolve = stubAlbum.GetAlbumByName("Evolve") ?? new Album("Evolve", "album11.png", ImagineDragons, "desc", "infos");
            Album SmokeAndMirrors = stubAlbum.GetAlbumByName("Smoke & Mirrors") ?? new Album("Smoke & Mirrors", "album11.png", ImagineDragons, "desc", "infos");
            Album NightVisions = stubAlbum.GetAlbumByName("Night Visions") ?? new Album("Night Visions", "album11.png", ImagineDragons, "desc", "infos");
            Album AB = stubAlbum.GetAlbumByName("Adios Bahamas") ?? new Album("Adios Bahamas", "album1.jpg", Nepal, "Album post-mortem qui signé également le dernier de l'artiste", "Sortie : 2020");
            Album E445 = stubAlbum.GetAlbumByName("445e Nuit") ?? new Album("445e Nuit", "album2.jpg", Nepal, "", "Sortie : 2017\n8 titres - 24 min");
            Album FSR = stubAlbum.GetAlbumByName("Fenetre Sur Rue") ?? new Album("Fenêtre Sur Rue", "album3.jpg", Hugo, "", "Sortie : 2012\n14 titres - 46 min");
            Album TM = stubAlbum.GetAlbumByName("Temps Mort") ?? new Album("Temps Mort", "album4.jpg", Booba, "Premier album de Booba", "Sortie : 2002\n14 titres - 57 min");
            Album OP = stubAlbum.GetAlbumByName("Opéra Puccino") ?? new Album("Opéra Puccino", "album5.jpg", Oxmo, "", "Sortie : 1998\n18 titres - 1h08min");
            Album EMA = stubAlbum.GetAlbumByName("L'école du micro d'argent") ?? new Album("L'école du micro d'argent", "album6.jpg", IAM, "", "Sortie : 1997\n16 titres - 1h13min");
            Album DF = stubAlbum.GetAlbumByName("Deux Frères") ?? new Album("Deux Frères", "album7.png", PNL, "", "Sortie : 2019\n22 titres");
            Album DLL = stubAlbum.GetAlbumByName("Dans la légende") ?? new Album("Dans la légende", "album8.jpg", PNL, "", "Sortie : 2016\n16 titres - 1h06");
            infoTitles = new ObservableCollection<InfoTitle>()
            {
                new InfoTitle("Bones", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "La chanson \"Bones\" a été publiée en tant que premier single de Mercury - Act 2 le 11 mars 2022. La chanson a été utilisée pour promouvoir la troisième saison de la série Amazon Prime Video The Boys. La sortie du clip de la chanson le 6 avril a coïncidé avec la précommande de l'album.", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Symphony", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "\"Symphony\" a été envoyé à la radio en Italie le 25 novembre 2022, en tant que quatrième single de l'album.", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Sharks", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "Le deuxième single de l'album, \"Sharks\", est sorti le 24 juin 2022, accompagné d'un clip vidéo. Il a été envoyé aux radios italiennes le 1er juillet 2022.", Genre.POP, MercuryAct2.ID),
                new InfoTitle("I don't like myself", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "Le troisième single de l'album, \"I Don't Like Myself\", est sorti le 10 octobre 2022, accompagné d'un clip vidéo pour la Journée mondiale de la santé mentale. À cette occasion, le groupe s'est associé à Crisis Text Line pour une campagne de collecte de fonds.", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Blur", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Higher ground", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Crushed", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "\"Crushed\" a été annoncé comme le cinquième single, avec une date de sortie fixée au 10 mai 2023. Le clip vidéo qui l'accompagne a été publié à la même date. La vidéo a été filmée en Ukraine en partenariat avec United24.", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Take it easy", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Waves", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("I'm happy", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Ferris wheel", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Peace of mind", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Sirens", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Tied", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Younger", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("Continual", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),
                new InfoTitle("They don't know you like I do", MercuryAct2.ImageURL, "Titre de l'album Mercury Act 2", "desc", Genre.POP, MercuryAct2.ID),


                new InfoTitle("Enemy", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("My life", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("Lonely", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("Wrecked", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("Monday", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("#1", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("Easy come easy go", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("Giants", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("It's ok", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("Dull knives", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("Follow you", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("Cutthroat", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("No time for toxic people", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),
                new InfoTitle("One day", MercuryAct1.ImageURL, "infos", "desc", Genre.POP, MercuryAct1.ID),

                new InfoTitle("Natural", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("Boomerang", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("Machine", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("Cool out", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("Bad Liar", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("West coast", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("Zero", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("Bullet in a gun", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("Digital", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("Only", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("Stuck", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),
                new InfoTitle("Love", Origins.ImageURL, "infos", "desc", Genre.POP, Origins.ID),

                new InfoTitle("I don't know why", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),
                new InfoTitle("Whatever it takes", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),
                new InfoTitle("Believer", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),
                new InfoTitle("Walking the wire", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),
                new InfoTitle("Rise up", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),
                new InfoTitle("I'll make it up to you", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),
                new InfoTitle("Yesterday", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),
                new InfoTitle("Mouth of the river", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),
                new InfoTitle("Thunder", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),
                new InfoTitle("Start over", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),
                new InfoTitle("Dancing in the dark", Evolve.ImageURL, "infos", "desc", Genre.POP, Evolve.ID),

                new InfoTitle("Shots", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Gold", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Smoke and Mirrors", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("I'm so sorry", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("I bet my life", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Polaroid", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Friction", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("It comes back to you", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Dream", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Trouble", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Summer", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Hopeless Opus", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("The fall", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Thief", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("The Unknown", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Second chances", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Release", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),
                new InfoTitle("Warriors", SmokeAndMirrors.ImageURL, "infos", "desc", Genre.POP, SmokeAndMirrors.ID),

                new InfoTitle("Radioactive", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Tiptoe", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("It's time", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Demons", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("On top of the world", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Amsterdam", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Hear me", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Every night", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Bleeding out", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Underdog", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Nothing left to say / Rocks", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Cha-ching (till we grow older)", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Working man", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("My fault", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Round and round", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("The river", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("America", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Selene", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Fallen", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Cover up", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Love of mine", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Bubble", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),
                new InfoTitle("Tokyo", NightVisions.ImageURL, "infos", "desc", Genre.POP, NightVisions.ID),

                new InfoTitle("Opening", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("Ennemis, Pt. 2", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("En face", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("Trajectoire", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("Vibe", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("Lemonade", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("Là-bas", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("Sundance", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("Milionaire", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("Sans voir", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("Crossfader", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),
                new InfoTitle("Daruma", AB.ImageURL, "infos", "desc", Genre.HIP_HOP, AB.ID),

                new InfoTitle("Niveau 1", E445.ImageURL, "infos", "desc", Genre.HIP_HOP, E445.ID),
                new InfoTitle("Maladavexa", E445.ImageURL, "infos", "desc", Genre.HIP_HOP, E445.ID),
                new InfoTitle("Love64 (Interlude)", E445.ImageURL, "infos", "desc", Genre.HIP_HOP, E445.ID),
                new InfoTitle("Deadpornstars", E445.ImageURL, "infos", "desc", Genre.HIP_HOP, E445.ID),
                new InfoTitle("Jugements", E445.ImageURL, "infos", "desc", Genre.HIP_HOP, E445.ID),
                new InfoTitle("Insomnie", E445.ImageURL, "infos", "desc", Genre.HIP_HOP, E445.ID),
                new InfoTitle("Kodak White", E445.ImageURL, "infos", "desc", Genre.HIP_HOP, E445.ID),
                new InfoTitle("Kamehouse", E445.ImageURL, "infos", "desc", Genre.HIP_HOP, E445.ID),

                new InfoTitle("Temps Mort", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("Independants", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("Ecoute bien", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("Ma définition", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("Jusqu'ici tout va bien", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("Repose en paix", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("Le Bitume avec une plume", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("Animals", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("Sans ratures", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("100-8 zoo", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("On m'a dit", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("Nouvelle école", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("De mauvaise augure", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),
                new InfoTitle("Strass et paillettes", TM.ImageURL, "infos", "desc", Genre.HIP_HOP, TM.ID),

                new InfoTitle("Visions de vie", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Black Mafioso (Interlude)", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Hitman", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Qui peut le nier !", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Peur noire", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("L'enfant seul", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Alias Jon Smoke", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Peu de gens le savent (Interlude)", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Amour & jalousie", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("24 heures à vivre", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Sacré samedi soir", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Le jour où tu partiras", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Sortilège", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Black Cyrano de Bergerac (Interlude)", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Mensongeur", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("La lettre", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("La loi du point final", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),
                new InfoTitle("Mourir 1000 fois", OP.ImageURL, "infos", "desc", Genre.HIP_HOP, OP.ID),

                new InfoTitle("L'école du micro d'argent", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Dangereux", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Nés sous la même étoile", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("La Saga", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Petit frère", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Elle donne son corps avant son nom", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("L'empire du côté obscur", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Regarde", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("L'Enfer", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Quand tu allais, on revenait", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Chez le mac", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Un bon son brut pour les truands", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Bouger la tête", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Un cri court dans la nuit", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Libère mon imagination", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),
                new InfoTitle("Demain, c'est loin", EMA.ImageURL, "infos", "desc", Genre.HIP_HOP, EMA.ID),

                new InfoTitle("Au DD", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Autre monde", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Chang", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Blanka", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("91's", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("À l'ammoniaque", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Celsius", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Deux frères", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Hasta la Vista", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Coeurs", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Shenmue", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Kuta Ubud", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Menace", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Zoulou Tchaing", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("Déconnecté", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),
                new InfoTitle("La misère est si belle", DF.ImageURL, "infos", "desc", Genre.HIP_HOP, DF.ID),

                new InfoTitle("DA", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Naha", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Dans la légende", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Mira", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("J'suis QLF", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("La vie est belle", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Kratos", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Luz de Luna", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Tu sais pas", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Sheita", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Humain", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Bambina", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Bené", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Uranus", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Onizuka", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),
                new InfoTitle("Jusqu'au dernier gramme", DLL.ImageURL, "infos", "desc", Genre.HIP_HOP, DLL.ID),

                new InfoTitle("Point de départ", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Ugotrip", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Alors dites pas", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Coma artificiel", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Fenêtre sur rue", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("La ligne verte", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Eldorado", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Interlude", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Dojo", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Piège à loup", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Intact", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Dégradation", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Old Boy", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID),
                new InfoTitle("Point final", FSR.ImageURL, "infos", "desc", Genre.HIP_HOP, FSR.ID)
            };
        }

        /// <summary>
        /// Permet d'obtenir une liste des InfoTitle
        /// </summary>
        /// <returns>Retourne une liste des InfoTitle</returns>
        public ObservableCollection<InfoTitle> GetInfoTitles()
        {
            return infoTitles;
        }

        /// <summary>
        /// Permet d'obtenir des InfoTitle selon les noms donnés en paramètre
        /// </summary>
        /// <param name="names">Liste des noms des InfoTitle recherchés</param>
        /// <returns>Retourne la liste des InfoTitle correspondant aux noms passés en paramètre</returns>
        public List<InfoTitle> GetInfoTitlesByNames(List<string> names)
        {
            List<InfoTitle> infos = new List<InfoTitle>();
            foreach (var name in names)
            {
                foreach (var titles in infoTitles)
                {
                    if (name == titles.Name)
                    {
                        infos.Add(titles);
                    }
                }
            }
            return infos;
        }

        /// <summary>
        /// Permet d'ajouter un InfoTitle à la liste des InfoTitle
        /// </summary>
        /// <param name="title">InfoTitle à ajouter</param>
        public void AddInfoTitle(InfoTitle title)
        {
            infoTitles.Add(title);
        }

        /// <summary>
        /// Permet de retirer un InfoTitle de la liste des InfoTitle
        /// </summary>
        /// <param name="title">InfoTitle à retirer</param>
        public void RemoveInfoTitle(InfoTitle title)
        {
            infoTitles.Remove(title);
        }

        /// <summary>
        /// Permet d'ajouter un artiste à la liste des feat d'un InfoTitle
        /// </summary>
        /// <param name="infoTitle">InfoTitle dans lequel ajouter le feat</param>
        /// <param name="artist">Artist à ajouter dans la liste des feats de l'InfoTitle</param>
        public static void AddFeat(InfoTitle infoTitle, Artist artist)
        {
            infoTitle.AddFeat(artist);
        }

        /// <summary>
        /// Permet de retirer un artiste de la liste des feat d'un InfoTitle
        /// </summary>
        /// <param name="infoTitle">InfoTitle depuis lequel retirer le feat</param>
        /// <param name="artist">Artist à retirer de la liste des feats de l'InfoTitle</param>
        public static void RemoveFeat(InfoTitle infoTitle, Artist artist)
        {
            infoTitle.RemoveFeat(artist);
        }
    }
}