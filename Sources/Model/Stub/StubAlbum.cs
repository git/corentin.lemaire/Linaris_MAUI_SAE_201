﻿using System.Collections.ObjectModel;

namespace Model.Stub
{
    public class StubAlbum
    {
        public StubArtist StubArtist
        {
            get
            {
                return stubArtist;
            }
        }

        private readonly StubArtist stubArtist;

        public ObservableCollection<Album> Albums
        {
            get => albums;
        }

        private readonly ObservableCollection<Album> albums;

        /// <summary>
        /// Constructeur de la classe StubAlbum
        /// </summary>
        public StubAlbum()
        {
            stubArtist = new StubArtist();
            albums = new ObservableCollection<Album>()
            {
                new Album("Adios Bahamas", "album1.jpg", StubArtist.GetArtistByName("Nepal") ?? new Artist("Nepal"), "Album post-mortem qui signé également le dernier de l'artiste", "Sortie : 2020"),
                new Album("445e Nuit", "album2.jpg", StubArtist.GetArtistByName("Nepal") ?? new Artist("Nepal"), "", "Sortie : 2017\n8 titres - 24 min"),
                new Album("Fenetre Sur Rue", "album3.jpg", StubArtist.GetArtistByName("Hugo TSR") ?? new Artist("Hugo TSR"), "", "Sortie : 2012\n14 titres - 46 min"),
                new Album("Temps Mort", "album4.jpg", StubArtist.GetArtistByName("Booba") ?? new Artist("Booba"), "Premier album de Booba", "Sortie : 2002\n14 titres - 57 min"),
                new Album("Opéra Puccino", "album5.jpg", StubArtist.GetArtistByName("Oxmo Puccino") ?? new Artist("Oxmo Puccino"), "", "Sortie : 1998\n18 titres - 1h08min"),
                new Album("L'école du micro d'argent", "album6.jpg", StubArtist.GetArtistByName("IAM") ?? new Artist("IAM"), "", "Sortie : 1997\n16 titres - 1h13min"),
                new Album("Deux Frères", "album7.png", StubArtist.GetArtistByName("PNL") ?? new Artist("PNL"), "", "Sortie : 2019\n22 titres"),
                new Album("Dans la légende", "album8.jpg", StubArtist.GetArtistByName("PNL") ?? new Artist("PNL"), "", "Sortie : 2016\n16 titres - 1h06"),
                new Album("Night Visions", "album9.jpg", StubArtist.GetArtistByName("Imagine Dragons") ?? new Artist("Imagine Dragons"), "Premier album d'Imagine Dragons", "Night Visions est le premier album studio du groupe de rock alternatif américain Imagine Dragons, sorti le 5 septembre 2012\nÀ sa sortie aux États-Unis, l'album entre directement en seconde position du Billboard 200, s'écoulant à plus de 83 000 unités en une semaine. Il a également atteint la première position du Billboard Alternative Album Chart et du Billboard Rock Albums Chart, ainsi que la dixième position au Canada."),
                new Album("Smoke & Mirrors", "album10.jpg", StubArtist.GetArtistByName("Imagine Dragons") ?? new Artist("Imagine Dragons"), "Deuxième album d'Imagine Dragons", "Smoke + Mirrors est le deuxième album studio du groupe de rock alternatif américain Imagine Dragons, sorti le 17 février 2015."),
                new Album("Evolve", "album11.jpg", StubArtist.GetArtistByName("Imagine Dragons") ?? new Artist("Imagine Dragons"), "Troisième album d'Imagine Dragons", "Evolve (stylisé ƎVOLVE) est le troisième album studio du groupe de rock alternatif américain Imagine Dragons sorti le 23 juin 2017."),
                new Album("Origins", "album12.jpg", StubArtist.GetArtistByName("Imagine Dragons") ?? new Artist("Imagine Dragons"), "Quatrième album d'Imagine Dragons", "Origins est le quatrième album studio du groupe de rock alternatif américain Imagine Dragons, sorti le 9 novembre 2018."),
                new Album("Mercury Act 1", "album13.jpg", StubArtist.GetArtistByName("Imagine Dragons") ?? new Artist("Imagine Dragons"), "Première partie du cinquième album d'Imagine Dragons", "Mercury - Act 1 est le cinquième album studio du groupe de pop rock américain Imagine Dragons, sorti le 3 septembre 2021 par Kidinakorner et Interscope Records aux États-Unis."),
                new Album("Mercury Act 2", "album14.jpg", StubArtist.GetArtistByName("Imagine Dragons") ?? new Artist("Imagine Dragons"), "Deuxième partie du cinquième album d'Imagine Dragons", "Mercury – Acts 1 & 2 est le cinquième album studio du groupe de rock alternatif américain Imagine Dragons. Il s'agit d'un double album, dont le premier acte est sorti le 3 septembre 2021 et le second le 1 juillet 2022.")
            };
        }

        /// <summary>
        /// Permet d'obtenir la liste des albums
        /// </summary>
        /// <returns>Retourne la liste des albums</returns>
        public ObservableCollection<Album> GetAlbums()
        {
            return albums;
        }

        /// <summary>
        /// Permet d'obtenir un album selon son nom
        /// </summary>
        /// <param name="name">Nom de l'album recherché</param>
        /// <returns>Retourne l'album correspondant au nom donné en paramètre</returns>
        public Album GetAlbumByName(string name)
        {
            foreach (Album album in albums)
            {
                if (name == album.Name)
                {
                    return album;
                }
            }
            return null;
        }

        /// <summary>
        /// Permet d'ajouter un album à la liste des albums
        /// </summary>
        /// <param name="album">Album à ajouter</param>
        public void AddAlbum(Album album)
        {
            albums.Add(album);
        }

        /// <summary>
        /// Permet de retirer un album de la liste des albums
        /// </summary>
        /// <param name="album">Album à retirer</param>
        public void RemoveAlbum(Album album)
        {
            albums.Remove(album);
        }
    }
}