﻿namespace Model
{
    /// <remarks>
    /// Enum des diffénres genres référencés.
    /// </remarks>
    public enum Genre
    {
        HIP_HOP, POP, ROCK, ELECTRO, CLASSIQUE, JAZZ, VARIETE_FRANCAISE, VARIETE_INTERNATIONALE, REGGAE, RAP, RNB, DISCO, BLUES, COUNTRY, FUNK, GOSPEL,
        METAL, K_POP
    }
}